import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/gallery_page/gallery_page.dart';

class CreateStoryPage extends StatelessWidget {
  static const String route = 'create-story-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FlatButton(
          child: Text('Create Story'),
          onPressed: () => Navigator.pushNamed(context, GalleryPage.route),
        ),
      ),
    );
  }
}
