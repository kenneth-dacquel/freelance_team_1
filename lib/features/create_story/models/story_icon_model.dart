import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class StoryIconModel {
  StoryIconModel({
    @required this.onTap,
    this.asset,
  });

  SvgPicture asset;
  VoidCallback onTap;
}
