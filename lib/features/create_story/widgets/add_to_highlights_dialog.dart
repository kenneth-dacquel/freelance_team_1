import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/create_story/widgets/highlights_row.dart';
import 'package:freelance_team_1/features/create_story/widgets/new_highlight_dialog.dart';
import 'package:freelance_team_1/utilities/bottom_sheet_creator.dart';
import 'package:freelance_team_1/widgets/white_button.dart';

class AddToHighlightsDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 375,
      padding: const EdgeInsets.only(
        left: 15,
        top: 34,
        bottom: 20,
        right: 15,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      child: Column(
        children: [
          Text(
            'Add to highlights',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: Colors.black,
              letterSpacing: 0,
            ),
          ),
          SizedBox(height: 34),
          SizedBox(
            height: 162,
            child: HighlightsRow(
              onNewHighlightPressed: () => BottomSheetCreator(
                context: context,
                dialog: NewHighlightDialog(),
                barrierColor: Colors.transparent,
              ).showBottomSheet(),
            ),
          ),
          SizedBox(height: 40),
          WhiteButton(
            onTap: () => Navigator.pop(context),
            label: 'Cancel',
          ),
        ],
      ),
    );
  }
}
