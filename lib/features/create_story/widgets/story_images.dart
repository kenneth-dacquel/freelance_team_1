import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class StoryImages extends StatelessWidget {
  final stories = [
    AssetImages.storyThumbnail2,
    AssetImages.storyThumbnail,
    AssetImages.storyThumbnail2,
    AssetImages.storyThumbnail2,
    AssetImages.storyThumbnail2,
    AssetImages.storyThumbnail2,
  ];
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.only(left: 10),
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        return Container(
          width: 47,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: index == 1 ? Colors.white : Colors.transparent,
              width: 1,
            ),
            image: DecorationImage(
              image: AssetImage(stories[index]),
              fit: BoxFit.fill,
            ),
          ),
        );
      },
      separatorBuilder: (context, index) => SizedBox(width: 5),
      itemCount: stories.length,
    );
  }
}
