import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class TaggedPerson extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38,
      width: 170,
      padding: const EdgeInsets.only(
        left: 10,
        right: 15,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        color: chipDark.withOpacity(0.7),
      ),
      child: Row(
        children: [
          Image.asset(
            AssetImages.taggedImage,
            height: 26,
            width: 26,
          ),
          SizedBox(width: 10),
          Expanded(
            child: Text(
              '@Nandagopal I',
              style: TextStyle(
                fontSize: 14,
                color: Colors.white,
                fontWeight: FontWeight.w600,
                letterSpacing: 0,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
