import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/create_story/models/highlights.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class HighlightsRow extends StatelessWidget {
  const HighlightsRow({@required this.onNewHighlightPressed});

  final VoidCallback onNewHighlightPressed;
  @override
  Widget build(BuildContext context) {
    final highlights = [
      Highlights(
        image: AssetImages.highlightImage,
        place: 'Dubai',
      ),
      Highlights(
        image: AssetImages.storyThumbnail2,
        place: 'Ooty',
      ),
      Highlights(
        image: AssetImages.storyThumbnail,
        place: 'Kerala',
      ),
      Highlights(
        image: AssetImages.storyThumbnail2,
        place: 'New York',
      ),
    ];
    final textStyle = TextStyle(
      fontWeight: FontWeight.w400,
      fontSize: 14,
      color: Colors.black,
      letterSpacing: 0,
    );

    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Column(
            children: [
              InkWell(
                onTap: onNewHighlightPressed,
                child: Container(
                  height: 120,
                  width: 93,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      color: chipGrey.withOpacity(0.4),
                      width: 1,
                    ),
                  ),
                  child: Center(
                    child: SvgPicture.asset(
                      AssetImages.plusIcon,
                      color: green,
                      height: 29,
                      width: 29,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Text(
                'New',
                style: textStyle,
              )
            ],
          );
        } else {
          final highlight = highlights[index - 1];
          return Column(
            children: [
              Container(
                height: 120,
                width: 93,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(highlight.image),
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              SizedBox(height: 15),
              Text(
                highlight.place,
                style: textStyle,
              )
            ],
          );
        }
      },
      separatorBuilder: (context, index) => SizedBox(width: 15),
      itemCount: highlights.length + 1,
    );
  }
}
