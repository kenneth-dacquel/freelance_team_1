import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:freelance_team_1/features/create_story/models/story_icon_model.dart';

class StoryIcons extends StatelessWidget {
  final List<StoryIconModel> children;

  const StoryIcons({
    Key key,
    this.children,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 20),
        ...?children.map((e) => Column(
              children: [
                IconButton(
                  padding: EdgeInsets.zero,
                  iconSize: 38,
                  onPressed: e.onTap,
                  icon: e.asset,
                ),
                SizedBox(height: 5),
              ],
            )),
      ],
    );
  }
}
