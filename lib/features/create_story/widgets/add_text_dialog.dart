import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:freelance_team_1/features/create_story/widgets/color_palette_item.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class AddTextDialog extends StatefulWidget {
  @override
  _AddTextDialogState createState() => _AddTextDialogState();
}

class _AddTextDialogState extends State<AddTextDialog> {
  double sliderValue = 10;
  Color textColor = Colors.white;
  bool showColorPicker = false;

  void setTextColor(Color color) {
    setState(() {
      textColor = color;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          Container(
            color: Colors.black,
            height: MediaQuery.of(context).size.height * 0.05,
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Color(0xFF26374A),
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0),
                ),
              ),
              child: Stack(
                fit: StackFit.expand,
                children: [
                  DragabbleEditableText(
                    textSize: sliderValue,
                    textColor: textColor,
                  ),
                  Positioned(
                    top: 10,
                    right: 0,
                    child: Column(
                      children: [
                        FlatButton(
                          onPressed: () => Navigator.pop(context),
                          child: Text(
                            'Done',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        IconButton(
                          icon: Image.asset(
                            AssetImages.colorsIcon,
                            height: 32,
                            width: 32,
                          ),
                          onPressed: () {
                            setState(() {
                              showColorPicker = !showColorPicker;
                            });
                          },
                        ),
                        IconButton(
                          icon: SvgPicture.asset(
                            AssetImages.alignCenterIcon,
                            height: 32,
                            width: 32,
                          ),
                          onPressed: () {},
                        ),
                        SizedBox(height: 60),
                        SizedBox(
                          height: 155,
                          child: FlutterSlider(
                            handlerHeight: 24,
                            handlerWidth: 24,
                            values: [sliderValue],
                            rtl: true,
                            max: 60,
                            min: 10,
                            axis: Axis.vertical,
                            onDragging: (handlerIndex, lowerValue, upperValue) {
                              sliderValue = lowerValue;
                              setState(() {});
                            },
                            handler: FlutterSliderHandler(
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(25),
                                ),
                              ),
                            ),
                            tooltip: FlutterSliderTooltip(disabled: true),
                            centeredOrigin: true,
                            trackBar: FlutterSliderTrackBar(
                              inactiveTrackBar: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                color: Colors.white.withOpacity(0.2),
                              ),
                              activeTrackBar: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                color: Colors.transparent,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (showColorPicker)
                    Positioned(
                      bottom: 30,
                      left: 0,
                      right: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(width: 20),
                          ColorPaletteItem(
                            color: Colors.white,
                            onTap: () => setTextColor(Colors.white),
                          ),
                          SizedBox(width: 20),
                          ColorPaletteItem(
                            color: Colors.black,
                            onTap: () => setTextColor(Colors.black),
                          ),
                          SizedBox(width: 20),
                          ColorPaletteItem(
                            color: Color(0xFFEAA41C),
                            onTap: () => setTextColor(Color(0xFFEAA41C)),
                          ),
                          SizedBox(width: 20),
                          ColorPaletteItem(
                            color: Color(0xFF1C87EA),
                            onTap: () => setTextColor(Color(0xFF1C87EA)),
                          ),
                          SizedBox(width: 20),
                          ColorPaletteItem(
                            color: Color(0xFF57AE00),
                            onTap: () => setTextColor(Color(0xFF57AE00)),
                          ),
                          IconButton(
                            icon: SvgPicture.asset(
                              AssetImages.closeIcon,
                              height: 30,
                              width: 30,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              setState(() {
                                showColorPicker = false;
                              });
                            },
                          )
                        ],
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class DragabbleEditableText extends StatefulWidget {
  final double textSize;
  final Color textColor;

  const DragabbleEditableText({
    Key key,
    this.textSize,
    this.textColor,
  }) : super(key: key);
  @override
  _DragabbleEditableTextState createState() => _DragabbleEditableTextState();
}

class _DragabbleEditableTextState extends State<DragabbleEditableText> {
  FocusNode textFocusNode;
  TextEditingController textEditingController;

  double xPosition = 50;
  double yPosition = 50;

  @override
  void initState() {
    textEditingController = TextEditingController();
    textFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    textFocusNode.dispose();
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Positioned(
      top: yPosition,
      left: xPosition,
      child: GestureDetector(
        onPanUpdate: (tapInfo) {
          setState(() {
            xPosition += tapInfo.delta.dx;
            yPosition += tapInfo.delta.dy;
          });
        },
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          width: MediaQuery.of(context).size.width,
          child: TextFormField(
            keyboardType: TextInputType.multiline,
            maxLines: null,
            autofocus: true,
            focusNode: textFocusNode,
            cursorColor: Colors.white,
            style: textTheme.headline3.copyWith(
              color: widget.textColor,
              fontSize: widget.textSize,
            ),
            controller: textEditingController,
            decoration: InputDecoration(
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
            ),
          ),
        ),
      ),
    );
  }
}
