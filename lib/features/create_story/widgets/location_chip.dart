import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class LocationChip extends StatelessWidget {
  LocationChip(this.location);

  final String location;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38,
      width: 176,
      padding: const EdgeInsets.only(
        left: 10,
        right: 15,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        color: chipDark.withOpacity(0.4),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SvgPicture.asset(AssetImages.mapPinIcon2),
          Text(
            location,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w400,
              letterSpacing: 0,
              color: Colors.white,
            ),
          ),
          SvgPicture.asset(
            AssetImages.closeIcon,
            height: 15,
            width: 15,
            color: Colors.white,
          )
        ],
      ),
    );
  }
}
