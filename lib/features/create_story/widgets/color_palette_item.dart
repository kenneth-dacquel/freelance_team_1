import 'package:flutter/material.dart';

class ColorPaletteItem extends StatelessWidget {
  final Color color;
  final VoidCallback onTap;

  const ColorPaletteItem({
    Key key,
    this.color,
    this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 32,
        width: 32,
        decoration: BoxDecoration(
          color: color,
          borderRadius: BorderRadius.circular(25),
          border: Border.all(
            color: Colors.white,
            width: 2,
          ),
        ),
      ),
    );
  }
}
