import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class StoryBottomButtons extends StatelessWidget {
  const StoryBottomButtons({
    @required this.onPostStory,
    @required this.onShare,
    @required this.onAdd,
  });

  final VoidCallback onPostStory;
  final VoidCallback onAdd;
  final VoidCallback onShare;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MaterialButton(
          onPressed: onAdd,
          color: Colors.white,
          child: SvgPicture.asset(
            AssetImages.plusIcon,
            height: 18,
            width: 18,
          ),
          padding: EdgeInsets.all(16),
          shape: CircleBorder(),
          height: 57,
        ),
        Container(
          height: 57,
          width: 142,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(52),
            color: Colors.white,
          ),
          child: FlatButton(
            onPressed: onPostStory,
            child: Text(
              'Post Story',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
                letterSpacing: 0,
              ),
            ),
          ),
        ),
        MaterialButton(
          onPressed: onShare,
          color: chipDark.withOpacity(0.4),
          child: SvgPicture.asset(
            AssetImages.sendIcon,
            height: 20,
            width: 20,
          ),
          padding: EdgeInsets.all(10),
          shape: CircleBorder(),
          height: 42,
        ),
      ],
    );
  }
}
