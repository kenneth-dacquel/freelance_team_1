import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/white_button.dart';

class NewHighlightDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 375,
      padding: const EdgeInsets.only(
        left: 15,
        top: 34,
        bottom: 20,
        right: 15,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () => Navigator.pop(context),
                child: SizedBox(
                  height: 20,
                  width: 20,
                  child: SvgPicture.asset(
                    AssetImages.backButtonIcon,
                    height: 18,
                    width: 9,
                  ),
                ),
              ),
              Text(
                'New highlight',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                  letterSpacing: 0,
                ),
              ),
              SizedBox(width: 20),
            ],
          ),
          SizedBox(height: 34),
          Column(
            children: [
              Container(
                padding: const EdgeInsets.all(3),
                height: 120,
                width: 93,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    color: chipGrey.withOpacity(0.4),
                    width: 1,
                  ),
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: AssetImage(AssetImages.storyBackgroundImage),
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15),
              Text(
                'Name',
                style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                  color: Colors.black.withOpacity(0.5),
                  letterSpacing: 0,
                ),
              ),
              SizedBox(height: 40),
              WhiteButton(
                onTap: () {
                  // ADD
                  Navigator.pop(context);
                },
                label: 'Add',
              ),
            ],
          ),
        ],
      ),
    );
  }
}
