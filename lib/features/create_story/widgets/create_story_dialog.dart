import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/create_story/models/story_icon_model.dart';
import 'package:freelance_team_1/features/create_story/widgets/add_text_dialog.dart';
import 'package:freelance_team_1/features/create_story/widgets/location_chip.dart';
import 'package:freelance_team_1/features/create_story/widgets/post_story_dialog.dart';
import 'package:freelance_team_1/features/create_story/widgets/story_bottom_buttons.dart';
import 'package:freelance_team_1/features/create_story/widgets/story_icons.dart';
import 'package:freelance_team_1/features/create_story/widgets/tagged_person.dart';
import 'package:freelance_team_1/features/story_settings/story_settings_page.dart';
import 'package:freelance_team_1/features/view_story/view_stories_page.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/bottom_sheet_creator.dart';
import 'package:photo_manager/photo_manager.dart';

class CreateStoryDialog extends StatefulWidget {
  const CreateStoryDialog({this.assets});

  final List<AssetEntity> assets;
  @override
  _CreateStoryDialogState createState() => _CreateStoryDialogState();
}

class _CreateStoryDialogState extends State<CreateStoryDialog> {
  void showAddTextModal(BuildContext context) => showModalBottomSheet<void>(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: Colors.black,
        builder: (context) => AddTextDialog(),
        isScrollControlled: true,
        enableDrag: false,
      );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.black,
          height: MediaQuery.of(context).size.height * 0.05,
        ),
        Expanded(
          child: FutureBuilder<Uint8List>(
            future: widget.assets.first.thumbData,
            builder: (_, snapshot) {
              final bytes = snapshot.data;
              if (bytes == null)
                return Center(
                  child: SizedBox(
                    height: 20,
                    width: 20,
                    child: CircularProgressIndicator(),
                  ),
                );
              else
                // If wrong implementation this container should be the child of Expanded
                // no need for Future builder. Plus image under DecorationImage should be
                // AssetImage(name of the asset).
                return Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    image: DecorationImage(
                      // image: AssetImage(AssetImages.storyBackgroundImage),
                      image: MemoryImage(bytes),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(30.0),
                      topRight: Radius.circular(30.0),
                    ),
                  ),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10,
                    vertical: 20,
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          IconButton(
                            onPressed: () => Navigator.pop(context),
                            icon: SvgPicture.asset(
                              AssetImages.closeIcon,
                              color: Colors.white,
                            ),
                          ),
                          LocationChip('Bali, Indonesia...'),
                          IconButton(
                            onPressed: () => Navigator.pushNamed(context, StorySettingsPage.route),
                            icon: SvgPicture.asset(
                              AssetImages.settingsIcon,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      Align(
                        alignment: Alignment.centerRight,
                        child: StoryIcons(
                          children: [
                            StoryIconModel(
                              onTap: () => showAddTextModal(context),
                              asset: SvgPicture.asset(AssetImages.textIcon),
                            ),
                            StoryIconModel(
                              onTap: () => print('MAP PIN ICON'),
                              asset: SvgPicture.asset(AssetImages.mapPinIcon),
                            ),
                            StoryIconModel(
                              onTap: () => print('TAG ICON'),
                              asset: SvgPicture.asset(AssetImages.tagIcon),
                            ),
                            StoryIconModel(
                              onTap: () => print('DOWNLOAD ICON'),
                              asset: SvgPicture.asset(AssetImages.downloadIcon),
                            ),
                            StoryIconModel(
                              onTap: () => print('DELETE ICON'),
                              asset: SvgPicture.asset(AssetImages.deleteIcon),
                            ),
                          ],
                        ),
                      ),
                      // SizedBox(height: 118),
                      Spacer(),
                      Container(
                        padding: const EdgeInsets.only(left: 60),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Transform.rotate(angle: 75, child: TaggedPerson()),
                        ),
                      ),
                      Spacer(),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: StoryBottomButtons(
                          onPostStory: () => Navigator.pushReplacementNamed(
                            context,
                            ViewStoriesPage.route,
                            arguments: ViewStoriesPageArgs(isToReact: true),
                          ),
                          onShare: () => print('TEST'),
                          onAdd: () => BottomSheetCreator(
                            context: context,
                            dialog: PostStoryDialog(),
                            barrierColor: Colors.black,
                            before: () => Navigator.pop(context),
                          ).showBottomSheet(),
                        ),
                      ),
                    ],
                  ),
                );
            },
          ),
        ),
      ],
    );
  }
}
