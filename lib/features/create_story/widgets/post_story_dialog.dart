import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/create_story/models/story_icon_model.dart';
import 'package:freelance_team_1/features/create_story/widgets/story_icons.dart';
import 'package:freelance_team_1/features/story_settings/story_settings_page.dart';
import 'package:freelance_team_1/features/view_story/view_stories_page.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class PostStoryDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.black,
          height: MediaQuery.of(context).size.height * 0.05,
        ),
        Expanded(
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Color(0xFF433E33),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(30.0),
                topRight: Radius.circular(30.0),
              ),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 20,
            ),
            child: Stack(
              children: [
                Positioned(
                  top: 200,
                  left: 0,
                  right: 0,
                  child: Column(
                    children: [
                      UnconstrainedBox(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.asset(
                            AssetImages.storyBackgroundImage,
                            width: 183,
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      Text(
                        '😍',
                        style: TextStyle(fontSize: 40),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 180,
                  left: 0,
                  right: 0,
                  child: UnconstrainedBox(
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Row(
                        children: [
                          CircleAvatar(
                            child: Image.asset(AssetImages.member3),
                            maxRadius: 15,
                          ),
                          SizedBox(width: 5),
                          Text(
                            'Prashanth Pillai',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                          onPressed: () => Navigator.pop(context),
                          icon: SvgPicture.asset(
                            AssetImages.closeIcon,
                            color: Colors.white,
                          ),
                        ),
                        IconButton(
                          onPressed: () => Navigator.pushNamed(context, StorySettingsPage.route),
                          icon: SvgPicture.asset(
                            AssetImages.settingsIcon,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: StoryIcons(
                        children: [
                          StoryIconModel(
                            onTap: () => print('TEXT ICON'),
                            asset: SvgPicture.asset(AssetImages.textIcon),
                          ),
                          StoryIconModel(
                            onTap: () => print('MAP PIN ICON'),
                            asset: SvgPicture.asset(AssetImages.mapPinIcon),
                          ),
                          StoryIconModel(
                            onTap: () => print('TAG ICON'),
                            asset: SvgPicture.asset(AssetImages.tagIcon),
                          ),
                        ],
                      ),
                    ),
                    Spacer(),
                    MaterialButton(
                      height: 57,
                      minWidth: 142,
                      onPressed: () => Navigator.pushReplacementNamed(
                        context,
                        ViewStoriesPage.route,
                        arguments: ViewStoriesPageArgs(isToReact: false),
                      ),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(52),
                      ),
                      child: Text(
                        'Post Story',
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 0,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
