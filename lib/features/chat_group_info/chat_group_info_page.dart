import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/chat_group_attachment/chat_group_attachment_page.dart';
import 'package:freelance_team_1/features/chat_group_info/widgets/arrow_text.dart';
import 'package:freelance_team_1/features/chat_group_info/widgets/chat_group_info_header.dart';
import 'package:freelance_team_1/features/chat_group_info/widgets/chat_group_info_item.dart';
import 'package:freelance_team_1/features/chat_group_info/widgets/chat_member_item.dart';
import 'package:freelance_team_1/features/chat_group_info/widgets/media_images.dart';
import 'package:freelance_team_1/features/chat_group_info/widgets/theme_thumbnail.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';
import 'package:freelance_team_1/widgets/pravasi_app_bar.dart';

class ChatGroupInfoPage extends StatefulWidget {
  static const String route = 'chat-group-info-page';
  @override
  State<StatefulWidget> createState() => _ChatGroupInfoPageState();
}

class _ChatGroupInfoPageState extends State<ChatGroupInfoPage> {
  bool muteMessages = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PravasiAppBar(
        title: 'Group Info',
        leading: IconButton(
          icon: SvgPicture.asset(
            AssetImages.backButtonIcon,
            width: 9,
            height: 18,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          IconButton(
            padding: const EdgeInsets.only(right: 10),
            icon: SvgPicture.asset(
              AssetImages.ellipsis,
              width: 20,
              height: 20,
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: ListView(
        children: [
          ChatGroupInfoHeader(
            name: 'Family Group',
            description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            imageUrl: '',
          ),
          Divider(color: Colors.black45),
          ChatGroupInfoItem(
            title: 'Theme',
            trailing: ThemeThumbnail(),
            onPressed: null,
          ),
          ChatGroupInfoItem(
            padding: const EdgeInsets.symmetric(
              horizontal: 24,
              vertical: 5,
            ),
            title: 'Mute messages',
            trailing: Switch(
              activeColor: Colors.white,
              activeTrackColor: Colors.green,
              value: muteMessages,
              onChanged: (value) => setState(() => muteMessages = value),
            ),
            onPressed: null,
          ),
          ChatGroupInfoItem(
            title: 'Group settings',
            leading: SvgPicture.asset(AssetImages.settingsIcon),
            trailing: ArrowText(),
            onPressed: null,
          ),
          ChatGroupInfoItem(
            title: 'Media, Links and Docs',
            leading: SvgPicture.asset(AssetImages.mediaIcon),
            trailing: ArrowText(),
            withDivider: false,
            onPressed: () => Navigator.pushNamed(context, ChatGroupAttachmentPage.route),
          ),
          MediaImages(
            imageUrls: [
              'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
              'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
              'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
              'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
              'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
              'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
            ],
          ),
          Divider(
            indent: 24,
            endIndent: 24,
            color: Colors.black45,
          ),
          ChatGroupInfoItem(
            title: 'Saved messages',
            leading: SvgPicture.asset(AssetImages.starIcon),
            trailing: ArrowText(text: '12'),
            onPressed: null,
          ),
          ChatGroupInfoItem(
            title: 'Save to Camera roll',
            leading: SvgPicture.asset(AssetImages.saveMediaIcon),
            trailing: ArrowText(text: 'Always'),
            onPressed: null,
          ),
          ChatGroupInfoItem(
            padding: const EdgeInsets.only(
              left: 24,
              right: 24,
              top: 5,
            ),
            title: '256 members',
            trailing: AppBarButton(
              text: 'Add People',
              textColor: darkGreen,
              onPressed: null,
            ),
            withDivider: false,
            onPressed: null,
          ),
          ChatGroupInfoItem(
            title: 'Invite to group via link',
            leading: SvgPicture.asset(AssetImages.linkIcon),
            withDivider: false,
            onPressed: null,
          ),
          ChatMemberItem(
            fullName: 'Prince Xavier',
            username: 'bio',
            role: 'Admin',
            profileImageUrl: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg',
            onPressed: null,
          ),
          ChatMemberItem(
            fullName: 'Vishnu Vinayan',
            username: 'username',
            role: '',
            profileImageUrl: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg',
            onPressed: null,
          ),
          ChatMemberItem(
            fullName: 'Blex Aolton',
            username: 'number',
            role: '',
            profileImageUrl: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg',
            onPressed: null,
          ),
          ChatMemberItem(
            fullName: 'Hexi Lexi',
            username: 'online status',
            role: '',
            profileImageUrl: 'https://images.pexels.com/photos/220453/pexels-photo-220453.jpeg',
            onPressed: null,
          ),
          AppBarButton(
            text: 'See All',
            textColor: darkGreen,
            onPressed: null,
          ),
          Divider(
            indent: 24,
            endIndent: 24,
            color: Colors.black45,
          ),
          AppBarButton(
            text: 'Leave Group',
            textColor: Color(0xffE34E51),
            onPressed: null,
          ),
        ],
      ),
    );
  }
}
