import 'package:flutter/material.dart';
import 'package:freelance_team_1/widgets/thumbnail_image.dart';

class MediaImages extends StatelessWidget {
  const MediaImages({this.imageUrls});

  final List<String> imageUrls;

  @override
  Widget build(BuildContext context) {
    final children = imageUrls
        .map((url) => ThumbnailImage(
              imageUrl: url,
              width: 80,
              height: 80,
              radius: 10,
            ))
        .toList();
    return Container(
      padding: const EdgeInsets.symmetric(
        horizontal: 24,
        vertical: 10,
      ),
      height: 90,
      child: ListView.separated(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => children[index],
        separatorBuilder: (context, index) => SizedBox(width: 10),
        itemCount: children.length,
      ),
    );
  }
}
