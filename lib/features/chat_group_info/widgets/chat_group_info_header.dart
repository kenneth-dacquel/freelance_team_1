import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ChatGroupInfoHeader extends StatelessWidget {
  const ChatGroupInfoHeader({
    this.name,
    this.description,
    this.imageUrl,
  });

  final String name;
  final String description;
  final String imageUrl;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Padding(
      padding: const EdgeInsets.symmetric(
        vertical: 30,
        horizontal: 40,
      ),
      child: Stack(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 100,
                  height: 100,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.asset(
                      AssetImages.chatGroupInfoPlaceholder,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  name,
                  style: textTheme.headline5,
                ),
                SizedBox(height: 10),
                Text(
                  description,
                  style: textTheme.subtitle1,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: Opacity(
              opacity: 0.5,
              child: SvgPicture.asset(
                AssetImages.groupInfoEdit,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
