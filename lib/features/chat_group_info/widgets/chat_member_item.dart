import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ChatMemberItem extends StatelessWidget {
  const ChatMemberItem({
    @required this.onPressed,
    this.padding = const EdgeInsets.symmetric(
      horizontal: 24,
      vertical: 20,
    ),
    this.profileImageUrl,
    this.fullName,
    this.username,
    this.role,
    this.isVerified = false,
    this.withDivider = true,
  });

  final EdgeInsets padding;

  final String profileImageUrl;
  final String fullName;
  final String username;
  final String role;
  final bool withDivider;
  final bool isVerified;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Column(
      children: [
        InkWell(
          onTap: onPressed,
          child: Padding(
            padding: padding,
            child: Row(
              children: [
                SizedBox(
                  width: 50,
                  height: 50,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.network(
                      profileImageUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          fullName,
                          style: textTheme.button,
                        ),
                        SizedBox(width: 10),
                        SvgPicture.asset(AssetImages.verifiedIcon),
                      ],
                    ),
                    SizedBox(height: 10),
                    Text(
                      username,
                      style: textTheme.caption,
                    ),
                  ],
                ),
                if (role != null) ...[
                  Spacer(),
                  Text(
                    role,
                    style: textTheme.caption,
                  ),
                ],
              ],
            ),
          ),
        ),
      ],
    );
  }
}
