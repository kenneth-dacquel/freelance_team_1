import 'package:flutter/material.dart';

class ChatGroupInfoItem extends StatelessWidget {
  const ChatGroupInfoItem({
    @required this.onPressed,
    this.padding = const EdgeInsets.symmetric(
      horizontal: 24,
      vertical: 20,
    ),
    this.title,
    this.leading,
    this.trailing,
    this.withDivider = true,
  });

  final EdgeInsets padding;
  final String title;
  final Widget leading;
  final Widget trailing;
  final bool withDivider;
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Column(
      children: [
        InkWell(
          onTap: onPressed,
          child: Padding(
            padding: padding,
            child: Row(
              children: [
                if (leading != null) ...[
                  leading,
                  SizedBox(width: 10),
                ],
                Text(
                  title,
                  style: textTheme.button,
                ),
                if (trailing != null) ...[
                  Spacer(),
                  trailing,
                ],
              ],
            ),
          ),
        ),
        if (withDivider)
          Divider(
            indent: 24,
            endIndent: 24,
            color: Colors.black45,
          ),
      ],
    );
  }
}
