import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ArrowText extends StatelessWidget {
  const ArrowText({this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Row(
      children: [
        if (text != null)
          Text(
            text,
            style: textTheme.caption,
          ),
        SizedBox(width: 10),
        SvgPicture.asset(
          AssetImages.rightArrowIcon2,
          width: 5,
          height: 15,
        ),
      ],
    );
  }
}
