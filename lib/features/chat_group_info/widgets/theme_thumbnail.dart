import 'package:flutter/material.dart';

class ThemeThumbnail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Row(
      children: [
        Text(
          'Citrus',
          style: textTheme.caption,
        ),
        SizedBox(width: 10),
        Container(
          width: 36,
          height: 18,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(46),
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xff00A4AE),
                  Color(0xffFFBF1C),
                ],
              )),
        ),
      ],
    );
  }
}
