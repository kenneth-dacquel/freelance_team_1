import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GroupInfoItem extends StatelessWidget {
  const GroupInfoItem({
    this.item,
    this.caption,
    this.value,
  });
  final String item;
  final String caption;
  final String value;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: null,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 17),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  item,
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                if (caption != null)
                  Text(
                    caption,
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: vulcan.withOpacity(0.5),
                    ),
                  ),
              ],
            ),
            Row(
              children: [
                if (value != null) ...[
                  Text(
                    value,
                    style: TextStyle(
                      fontSize: 14,
                      color: vulcan.withOpacity(0.5),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(width: 20),
                ],
                SvgPicture.asset(
                  AssetImages.rightArrowIcon2,
                  height: 12,
                  width: 5,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
