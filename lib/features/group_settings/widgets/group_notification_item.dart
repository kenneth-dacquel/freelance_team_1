import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GroupNotificationItem extends StatefulWidget {
  const GroupNotificationItem(this.item);

  final String item;

  @override
  State<StatefulWidget> createState() => _GroupNotificationItemState();
}

class _GroupNotificationItemState extends State<GroupNotificationItem> {
  bool isActive = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 17),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.item,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                ),
              ),
              Text(
                description,
                style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: vulcan.withOpacity(0.5),
                ),
              ),
            ],
          ),
          InkWell(
            onTap: () => setState(() => isActive = !isActive),
            child: SvgPicture.asset(
              isActive ? AssetImages.activeCheckbox : AssetImages.inactiveCheckbox,
              height: 22,
              width: 22,
            ),
          ),
        ],
      ),
    );
  }
}
