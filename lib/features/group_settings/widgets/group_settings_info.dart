import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/constants.dart';

import 'group_info_item.dart';

class GroupSettingsInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Group Info',
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 20),
        GroupInfoItem(
          item: 'Name and description',
        ),
        GroupInfoItem(
          item: 'Group type',
          caption: description,
          value: 'General',
        ),
        GroupInfoItem(
          item: 'Location',
          caption: description,
          value: 'London, United Kin...',
        ),
        GroupInfoItem(
          item: 'Privacy',
          value: 'Private',
        ),
        GroupInfoItem(
          item: 'Hide group',
          value: 'Hidden',
        ),
      ],
    );
  }
}
