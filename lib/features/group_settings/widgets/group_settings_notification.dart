import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/group_settings/widgets/group_notification_item.dart';

class GroupSettingsNotification extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Notifications',
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
        SizedBox(height: 20),
        GroupNotificationItem('All post'),
        GroupNotificationItem('Your contacts and following'),
        GroupNotificationItem('Off'),
      ],
    );
  }
}
