import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/group_settings/widgets/group_settings_info.dart';
import 'package:freelance_team_1/features/group_settings/widgets/group_settings_notification.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class GroupSettingsPage extends StatelessWidget {
  static const String route = 'group-settings-page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset(AssetImages.closeIcon),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          'Group Settings',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          padding: const EdgeInsets.symmetric(
            vertical: 20,
            horizontal: 25,
          ),
          children: [
            GroupSettingsInfo(),
            SizedBox(height: 20),
            ListTile(
              contentPadding: EdgeInsets.zero,
              title: Text(
                'Group rules',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
              ),
              trailing: SvgPicture.asset(
                AssetImages.rightArrowIcon2,
                height: 12,
                width: 5,
              ),
            ),
            SizedBox(height: 30),
            GroupSettingsNotification(),
          ],
        ),
      ),
    );
  }
}
