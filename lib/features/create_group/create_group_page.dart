import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/add_members/models/member.dart';
import 'package:freelance_team_1/features/chat_group_info/chat_group_info_page.dart';
import 'package:freelance_team_1/features/create_group/widgets/create_group_header.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';

class CreateGroupPage extends StatefulWidget {
  static const String route = 'create-group-page';

  @override
  State<StatefulWidget> createState() => _CreateGroupPageState();
}

class _CreateGroupPageState extends State<CreateGroupPage> {
  final members = [
    Member(
      name: 'Vishnu Xavier',
      subtitle: 'number',
      image: AssetImages.member2,
      isJoined: false,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Muhammad Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: false,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Jay Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: false,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: true,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince Vinayan',
      subtitle: 'number',
      image: AssetImages.member2,
      isJoined: true,
      isContact: true,
      isCertified: true,
      isChecked: true,
    ),
    Member(
      name: 'Blex Aolton',
      subtitle: 'username',
      image: AssetImages.member3,
      isJoined: true,
      isContact: false,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince',
      subtitle: 'username',
      image: AssetImages.member4,
      isJoined: true,
      isContact: false,
      isCertified: false,
      isChecked: false,
    ),
  ];
  @override
  Widget build(BuildContext context) {
    final labelTextStyle = TextStyle(
      color: selectedChipBackgroundColor,
      fontSize: 16,
      fontWeight: FontWeight.w600,
      letterSpacing: 0,
    );
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          color: Colors.black,
          icon: SvgPicture.asset(AssetImages.closeIcon),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          'Create Group',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16,
            letterSpacing: 0,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: AppBarButton(
              text: 'Done',
              textColor: darkGreen,
              onPressed: () => Navigator.pushNamed(context, ChatGroupInfoPage.route),
            ),
          ),
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(vertical: 25),
        children: [
          CreateGroupHeader('Group Name'),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 25),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '${members.where((element) => element.isJoined).length} Members',
                  style: labelTextStyle,
                ),
                InkWell(
                  onTap: () {},
                  child: Text(
                    'Add People',
                    style: labelTextStyle.copyWith(color: darkGreen),
                  ),
                ),
              ],
            ),
          ),
          ...members
              .where((element) => element.isJoined)
              .map(
                (e) => ListTile(
                  leading: Image.asset(
                    e.image,
                    height: 50,
                    width: 50,
                  ),
                  title: Text(
                    e.name,
                    style: labelTextStyle.copyWith(color: vulcan),
                  ),
                  subtitle: Text(
                    e.subtitle,
                    style: TextStyle(
                      letterSpacing: 0,
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: vulcan.withOpacity(0.5),
                    ),
                  ),
                  trailing: IconButton(
                    onPressed: () => setState(() => e.isJoined = false),
                    icon: SvgPicture.asset(
                      AssetImages.closeIcon,
                      height: 20,
                      width: 20,
                    ),
                  ),
                ),
              )
              .toList(),
        ],
      ),
    );
  }
}
