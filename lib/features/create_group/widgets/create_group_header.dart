import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class CreateGroupHeader extends StatelessWidget {
  const CreateGroupHeader(this.name);

  final String name;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 100,
          height: 100,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Image.asset(
              AssetImages.chatGroupInfoPlaceholder,
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(height: 10),
        InkWell(
          onTap: () {},
          child: Text(
            'Change group picture',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: darkGreen,
              letterSpacing: 0,
            ),
          ),
        ),
        SizedBox(height: 20),
        Text(
          name,
          style: TextStyle(
            fontSize: 18,
            color: vulcan.withOpacity(0.4),
            fontWeight: FontWeight.w700,
            letterSpacing: 0,
          ),
        ),
        SizedBox(height: 30),
        Container(
          height: 1,
          width: MediaQuery.of(context).size.width,
          color: divider,
        ),
      ],
    );
  }
}
