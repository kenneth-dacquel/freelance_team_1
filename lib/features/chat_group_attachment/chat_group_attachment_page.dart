import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_docs.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_images.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_links.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_tab.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';

class ChatGroupAttachmentPage extends StatefulWidget {
  static const String route = 'chat-group-attachment';

  @override
  _ChatGroupAttachmentPageState createState() => _ChatGroupAttachmentPageState();
}

class _ChatGroupAttachmentPageState extends State<ChatGroupAttachmentPage> {
  int currentIndex;

  @override
  void initState() {
    currentIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Column(
            children: [
              Text(
                'Chalakkudy',
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Text(
                'Muncipality',
                style: TextStyle(
                  color: Colors.black.withOpacity(0.5),
                  fontSize: 12,
                ),
              ),
            ],
          ),
          leading: IconButton(
            icon: SvgPicture.asset(AssetImages.backButtonIcon),
            onPressed: () => Navigator.pop(context),
          ),
          actions: [
            if (currentIndex == 0)
              Padding(
                padding: const EdgeInsets.only(right: 15),
                child: AppBarButton(
                  text: 'Select',
                  textColor: darkGreen,
                  onPressed: () {},
                ),
              ),
            if (currentIndex == 2)
              IconButton(
                padding: const EdgeInsets.only(right: 15),
                icon: SvgPicture.asset(AssetImages.search),
                onPressed: () {},
              )
          ],
          bottom: TabBar(
            indicatorColor: darkGreen,
            labelColor: darkGreen,
            unselectedLabelColor: lightGrey,
            tabs: [
              ChatGroupAttachmentTab(title: 'Images'),
              ChatGroupAttachmentTab(title: 'Links'),
              ChatGroupAttachmentTab(title: 'Docs'),
            ],
            onTap: (index) => setState(() => currentIndex = index),
          ),
        ),
        body: TabBarView(
          children: [
            ChatGroupAttachmentImages(),
            ChatGroupAttachmentLinks(),
            ChatGroupAttachmentDocs(),
          ],
        ),
      ),
    );
  }
}
