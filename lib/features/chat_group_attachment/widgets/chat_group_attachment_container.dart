import 'package:flutter/material.dart';

class ChatGroupAttachmentLinkContainer extends StatelessWidget {
  const ChatGroupAttachmentLinkContainer({
    this.title,
    this.children,
  });

  final String title;
  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 4),
          child: Text(
            title,
            style: textTheme.button,
          ),
        ),
        SizedBox(height: 20),
        ...?children,
      ],
    );
  }
}
