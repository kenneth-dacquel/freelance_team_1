import 'package:flutter/material.dart';

class ChatGroupAttachmentTab extends StatelessWidget {
  const ChatGroupAttachmentTab({this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Tab(
      icon: Text(
        title,
        style: TextStyle(
          fontWeight: FontWeight.w600,
          fontSize: 16,
        ),
      ),
    );
  }
}
