import 'package:flutter/material.dart';
import 'package:freelance_team_1/widgets/thumbnail_image.dart';

class ChatGroupAttachmentImages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final images = [
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
      'https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg',
    ];
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
      itemBuilder: (context, index) => Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 1,
          vertical: 1,
        ),
        child: ThumbnailImage(
          imageUrl: images[index],
          width: 80,
          height: 80,
        ),
      ),
      itemCount: images.length,
    );
  }
}
