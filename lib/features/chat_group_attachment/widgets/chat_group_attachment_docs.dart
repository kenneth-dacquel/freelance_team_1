import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_container.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_docs_item.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ChatGroupAttachmentDocs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(20),
      children: [
        SizedBox(height: 10),
        ChatGroupAttachmentLinkContainer(
          title: '20 October 2020',
          children: [
            ChatGroupAttachmentDocsItem(
              imageUrl: AssetImages.chatGroupAttachmentDoc1,
              fileName: 'Banner Design.pdf',
              page: '4 pages',
              fileSize: '4 MB',
              fileType: 'PDF',
            ),
            ChatGroupAttachmentDocsItem(
              imageUrl: AssetImages.chatGroupAttachmentDoc2,
              fileName: '',
              page: '',
              fileSize: '',
              fileType: '',
            ),
          ],
        ),
      ],
    );
  }
}
