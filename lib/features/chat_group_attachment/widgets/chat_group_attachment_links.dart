import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_container.dart';
import 'package:freelance_team_1/features/chat_group_attachment/widgets/chat_group_attachment_link_item.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ChatGroupAttachmentLinks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(20),
      children: [
        SizedBox(height: 10),
        ChatGroupAttachmentLinkContainer(
          title: 'Today',
          children: [
            ChatGroupAttachmentLinkItem(
              imageUrl: AssetImages.chatGroupAttachmentLink1,
              title: 'Hacker News',
              description: 'Hacker News. Hacker News new | past | comments | ask | show | jobs | submit....',
              link: 'https://news.ycombinator.com/',
            ),
            ChatGroupAttachmentLinkItem(
              imageUrl: AssetImages.chatGroupAttachmentLink2,
              title: 'aji_ppaan',
              description: 'സ്വപ്നത്തിൽ ആരെങ്കിലും\n മീനായിട്ടുണ്ടോ?',
              link: 'https://www.instagram.com/p/B8v7I1GJRv...',
            ),
          ],
        ),
        SizedBox(height: 30),
        ChatGroupAttachmentLinkContainer(title: 'Yesterday'),
      ],
    );
  }
}
