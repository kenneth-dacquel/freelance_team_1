import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class ChatGroupAttachmentLinkItem extends StatelessWidget {
  const ChatGroupAttachmentLinkItem({
    this.imageUrl,
    this.title,
    this.description,
    this.link,
  });

  final String imageUrl;
  final String title;
  final String description;
  final String link;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Card(
      shadowColor: Color(0x80393939),
      elevation: 4,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(2),
              child: Image.asset(
                imageUrl,
                width: 52,
                height: 52,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: textTheme.button,
                  ),
                  SizedBox(height: 2),
                  Text(
                    description,
                    style: textTheme.subtitle1.copyWith(color: Colors.black54),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  SizedBox(height: 10),
                  Text(
                    link,
                    style: textTheme.subtitle1.copyWith(color: darkGreen),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
