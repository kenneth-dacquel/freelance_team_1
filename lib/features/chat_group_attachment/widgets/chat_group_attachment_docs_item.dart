import 'package:flutter/material.dart';

class ChatGroupAttachmentDocsItem extends StatelessWidget {
  const ChatGroupAttachmentDocsItem({
    this.imageUrl,
    this.fileName,
    this.page,
    this.fileSize,
    this.fileType,
  });

  final String imageUrl;
  final String fileName;
  final String page;
  final String fileSize;
  final String fileType;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Card(
      shadowColor: Color(0x80393939),
      elevation: 4,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(2),
              child: Image.asset(
                imageUrl,
                width: 52,
                height: 52,
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    fileName,
                    style: textTheme.button,
                  ),
                  SizedBox(height: 5),
                  if (fileSize?.isNotEmpty == true || page?.isNotEmpty == true || fileType?.isNotEmpty == true)
                    Text(
                      '$page • $fileSize • $fileType',
                      style: textTheme.subtitle1.copyWith(color: Colors.black54),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                    ),
                  SizedBox(height: 15),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
