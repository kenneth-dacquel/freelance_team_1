import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/add_members/add_members_page.dart';
import 'package:freelance_team_1/features/add_members/models/member.dart';
import 'package:freelance_team_1/features/add_members/widgets/members_section.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/enums.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';
import 'package:freelance_team_1/widgets/light_custom_text_field.dart';

class GroupMembersPage extends StatefulWidget {
  static const String route = 'group-members-page';
  @override
  State<StatefulWidget> createState() => _GroupMembersPageState();
}

class _GroupMembersPageState extends State<GroupMembersPage> {
  final members = [
    Member(
      name: 'Prince Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: false,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince Vinayan',
      subtitle: 'number',
      image: AssetImages.member2,
      isJoined: false,
      isContact: true,
      isCertified: true,
      isChecked: true,
    ),
    Member(
      name: 'Blex Aolton',
      subtitle: 'username',
      image: AssetImages.member3,
      isJoined: false,
      isContact: false,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince',
      subtitle: 'username',
      image: AssetImages.member4,
      isJoined: false,
      isContact: false,
      isCertified: false,
      isChecked: false,
    ),
  ];

  List<Member> joinedMembers;
  List<Member> filteredMembers;

  @override
  void initState() {
    joinedMembers = members.where((element) => element.isJoined).toList();
    filteredMembers = members.where((element) => element.isJoined == false).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: AppBarButton(
          text: 'Cancel',
          textColor: Colors.black,
          onPressed: () => Navigator.pop(context),
        ),
        leadingWidth: 80,
        title: Column(
          children: [
            Text(
              'Add Members',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
                letterSpacing: 0,
              ),
            ),
            Text(
              '1/number',
              style: TextStyle(
                color: vulcan.withOpacity(0.5),
                fontSize: 12,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: AppBarButton(
              text: 'Next',
              textColor: darkGreen,
              onPressed: () {},
            ),
          ),
        ],
      ),
      body: Container(
        child: ListView(
          padding: const EdgeInsets.symmetric(vertical: 20),
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: LightCustomTextField(
                onTap: () => Navigator.pushNamed(context, AddMembersPage.route),
                hint: 'Search members',
                onChanged: (text) {
                  final filtered =
                      members.where((element) => element.name.toLowerCase().contains(text.toLowerCase())).toList();
                  setState(() {
                    filteredMembers = filtered;
                  });
                },
              ),
            ),
            SizedBox(height: 15),
            MembersSection(
              type: MemberType.CONTACT,
              members: filteredMembers.where((element) => element.isContact).toList(),
            ),
            SizedBox(height: 5),
            MembersSection(
              type: MemberType.FOLLOWING,
              members: filteredMembers.where((element) => element.isContact == false).toList(),
            ),
          ],
        ),
      ),
    );
  }
}
