import 'package:flutter/material.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';
import 'package:freelance_team_1/features/add_group/widgets/create_group_section.dart';
import 'package:freelance_team_1/features/group_members/group_members_page.dart';
import 'package:freelance_team_1/utilities/app_router.dart';
import 'package:freelance_team_1/utilities/constants.dart';
import 'package:freelance_team_1/widgets/custom_text_field.dart';
import 'package:freelance_team_1/widgets/dark_green_button.dart';

class AddGroupPage extends StatefulWidget {
  static const String route = 'add-group-page';
  @override
  State<StatefulWidget> createState() => _AddGroupPageState();
}

class _AddGroupPageState extends State<AddGroupPage> {
  final children = [
    CustomTextField(label: 'Group Name'),
    CreateGroupSection(
      section: 'Location',
      sectionSubtitle: description,
      firstOption: 'Worldwide',
      firstOptionSubtitle: description,
      secondOption: 'Specific Location',
      secondOptionSubtitle: description,
    ),
    CustomTextField(label: 'Search Location'),
    CreateGroupSection(
      section: 'Privacy',
      firstOption: 'Public',
      firstOptionSubtitle: description,
      secondOption: 'Private',
      secondOptionSubtitle: description,
    ),
    SizedBox(height: 70),
    DarkGreenButton(
      onTap: () => Navigator.pushNamed(navigatorKey.currentContext, GroupMembersPage.route),
      title: 'Create Group',
      radius: 10.0,
    ),
    SizedBox(height: 30),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: AppBarButton(
          text: 'Cancel',
          textColor: Colors.black,
          onPressed: () => Navigator.pop(context),
        ),
        leadingWidth: 100,
        title: Text(
          'Create Group',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
          textAlign: TextAlign.center,
        ),
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(20),
        itemBuilder: (c, index) => children[index],
        separatorBuilder: (c, index) => SizedBox(height: 30),
        itemCount: children.length,
      ),
    );
  }
}
