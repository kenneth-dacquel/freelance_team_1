import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

import 'custom_list_tile.dart';

class CreateGroupSection extends StatefulWidget {
  const CreateGroupSection({
    this.section,
    this.sectionSubtitle,
    this.firstOption,
    this.firstOptionSubtitle,
    this.secondOption,
    this.secondOptionSubtitle,
  });

  final String section;
  final String sectionSubtitle;
  final String firstOption;
  final String firstOptionSubtitle;
  final String secondOption;
  final String secondOptionSubtitle;
  @override
  State<StatefulWidget> createState() => _CreateGroupSectionState();
}

class _CreateGroupSectionState extends State<CreateGroupSection> {
  bool isFirstOption = false;
  @override
  Widget build(BuildContext context) {
    final subtitleStyle = TextStyle(
      fontSize: 14,
      color: lightGrey.withOpacity(0.5),
    );
    return Padding(
      padding: const EdgeInsets.only(left: 5),
      child: Column(
        children: [
          CustomListTile(
            title: widget.section,
            subtitle: widget.sectionSubtitle,
            titleTextStyle: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            subtitleTextStyle: subtitleStyle,
          ),
          SizedBox(height: 33),
          CustomListTile(
            title: widget.firstOption,
            subtitle: widget.firstOptionSubtitle,
            titleTextStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
            subtitleTextStyle: subtitleStyle,
            suffx: InkWell(
              child: SvgPicture.asset(isFirstOption ? AssetImages.activeCheckbox : AssetImages.inactiveCheckbox),
              onTap: () => setState(() => isFirstOption = true),
            ),
          ),
          SizedBox(height: 25),
          CustomListTile(
            title: widget.secondOption,
            subtitle: widget.secondOptionSubtitle,
            titleTextStyle: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
            subtitleTextStyle: subtitleStyle,
            suffx: InkWell(
              child: SvgPicture.asset(isFirstOption ? AssetImages.inactiveCheckbox : AssetImages.activeCheckbox),
              onTap: () => setState(() => isFirstOption = false),
            ),
          )
        ],
      ),
    );
  }
}
