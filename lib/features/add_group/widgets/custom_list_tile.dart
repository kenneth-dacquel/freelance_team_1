import 'package:flutter/material.dart';

class CustomListTile extends StatelessWidget {
  const CustomListTile({
    this.title,
    this.subtitle,
    this.titleTextStyle,
    this.subtitleTextStyle,
    this.suffx,
  });

  final String title;
  final String subtitle;
  final TextStyle titleTextStyle;
  final TextStyle subtitleTextStyle;
  final Widget suffx;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: titleTextStyle,
            ),
            if (subtitle != null)
              Text(
                subtitle,
                style: subtitleTextStyle,
              ),
          ],
        ),
        if (suffx != null) suffx,
      ],
    );
  }
}
