import 'package:flutter/material.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';
import 'package:freelance_team_1/features/community/community_page.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/custom_dropdown.dart';
import 'package:freelance_team_1/widgets/custom_text_field.dart';

class AddAddressPage extends StatelessWidget {
  static const String route = 'add-address-page';
  @override
  Widget build(BuildContext context) {
    final children = [
      SizedBox(height: 30),
      Row(
        children: [
          SizedBox(width: 10),
          Flexible(
            child: Text(
              'Discover friends and groups from your locality',
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 17,
              ),
            ),
          ),
          SizedBox(width: 27),
          Image.asset(
            AssetImages.groupImage,
            width: 176,
            height: 79,
            fit: BoxFit.fill,
          ),
          SizedBox(width: 10),
        ],
      ),
      SizedBox(height: 32),
      CustomTextField(label: 'Country'),
      SizedBox(height: 26),
      CustomTextField(label: 'State'),
      SizedBox(height: 26),
      CustomDropdown(
        hint: 'District',
        items: ['District 1', 'District 2'],
        textBuilder: (item) => item,
      ),
      SizedBox(height: 26),
      Row(
        children: [
          Flexible(
            child: CustomDropdown(
              hint: 'Panchayath',
              items: ['Panchayath 1', 'Panchayath 2'],
              textBuilder: (item) => item,
            ),
            flex: 2,
          ),
          SizedBox(width: 15),
          Flexible(
            child: CustomDropdown(
              hint: 'Ward',
              items: ['Ward 1', 'Ward 2'],
              textBuilder: (item) => item,
            ),
          ),
        ],
      ),
      SizedBox(height: 8),
      Text(
        'Description goes here',
        style: TextStyle(
          fontSize: 13,
          fontWeight: FontWeight.w400,
          color: Color(0x8012273E),
        ),
      ),
      SizedBox(height: 26),
      CustomTextField(label: 'Home Address'),
      SizedBox(height: 26),
      CustomTextField(label: 'Zip'),
      SizedBox(height: 26),
    ];

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            AppBarButton(
              text: 'Cancel',
              textColor: Colors.black,
              onPressed: () {},
            ),
            Text(
              'Add address',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
              textAlign: TextAlign.center,
            ),
            AppBarButton(
              text: 'Done',
              textColor: darkGreen,
              onPressed: () => Navigator.pushNamed(context, CommunityPage.route),
            ),
          ],
        ),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        itemBuilder: (context, index) => children[index],
        itemCount: children.length,
      ),
    );
  }
}
