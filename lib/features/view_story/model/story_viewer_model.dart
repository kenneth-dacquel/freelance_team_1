import 'package:freelance_team_1/utilities/asset_images.dart';

class StoryViewerModel {
  final String name;
  final String username;
  final String imagePath;
  final String react;
  final bool verified;

  StoryViewerModel({
    this.name,
    this.username,
    this.imagePath,
    this.react = '',
    this.verified = false,
  });

  static List<StoryViewerModel> sampleData() => [
        StoryViewerModel(
          name: 'Prince Xavier',
          username: 'username',
          imagePath: AssetImages.member1,
          react: '👍',
        ),
        StoryViewerModel(
          name: 'Vishnu Vinayan',
          username: 'username',
          imagePath: AssetImages.member2,
          verified: true,
          react: '😂',
        ),
        StoryViewerModel(
          name: 'Blex Aolton',
          username: 'username',
          imagePath: AssetImages.member3,
          react: '😧',
        ),
        StoryViewerModel(
          name: 'Hexi Lexi',
          username: 'username',
          imagePath: AssetImages.member4,
          react: '🙏',
        ),
        StoryViewerModel(
          name: 'Vishnu Vinayan',
          username: 'username',
          imagePath: AssetImages.member2,
          verified: true,
        ),
        StoryViewerModel(
          name: 'Hexi Lexi',
          username: 'username',
          imagePath: AssetImages.member1,
        ),
      ];
}
