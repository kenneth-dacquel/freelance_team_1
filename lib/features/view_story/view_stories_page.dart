import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/view_story/widgets/story_view.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ViewStoriesPageArgs {
  const ViewStoriesPageArgs({this.isToReact});
  final bool isToReact;
}

class ViewStoriesPage extends StatefulWidget {
  static const route = 'view_stories_page';

  const ViewStoriesPage({this.args});

  final ViewStoriesPageArgs args;

  @override
  _ViewStoriesPageState createState() => _ViewStoriesPageState();
}

class _ViewStoriesPageState extends State<ViewStoriesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: StoryView(
        isToReact: widget.args.isToReact,
        stories: [
          Image.asset(
            AssetImages.storyBackgroundImage,
            fit: BoxFit.contain,
          ),
          Image.asset(
            AssetImages.storyBackgroundImage,
            fit: BoxFit.contain,
          ),
          Image.asset(
            AssetImages.storyBackgroundImage,
            fit: BoxFit.contain,
          ),
          Image.asset(
            AssetImages.storyBackgroundImage,
            fit: BoxFit.contain,
          ),
        ],
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}
