import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/story_settings/story_settings_page.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/white_button.dart';

class StoryMoreSheet extends StatelessWidget {
  StoryMoreSheet({this.isHighlight = false});

  final bool isHighlight;

  @override
  Widget build(BuildContext context) {
    final style = TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.w500,
      letterSpacing: 0,
    );
    final children = [
      if (isHighlight)
        ListTile(
          title: Text(
            'Edit Highlight',
            style: style,
          ),
          onTap: () {},
        ),
      ListTile(
        title: Text(
          'Save to Camera Roll',
          style: style,
        ),
        onTap: () {},
      ),
      ListTile(
        title: Text(
          'Send to',
          style: style,
        ),
        onTap: () {},
      ),
      ListTile(
        title: Text(
          'Copy Link',
          style: style,
        ),
        onTap: () {},
      ),
      ListTile(
        title: Text(
          'Share to',
          style: style,
        ),
        onTap: () {},
      ),
      ListTile(
        title: Text(
          'Story settings',
          style: style,
        ),
        onTap: () => Navigator.pushNamed(context, StorySettingsPage.route),
      ),
      ListTile(
        title: Text(
          isHighlight ? 'Remove from Highlight' : 'Delete',
          style: style.copyWith(color: bottomSheetRed),
        ),
        onTap: () {},
      ),
      WhiteButton(
        onTap: () => Navigator.pop(context),
        label: 'Cancel',
      ),
    ];
    return Container(
      height: isHighlight ? 650 : 550,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: ListView.separated(
        physics: NeverScrollableScrollPhysics(),
        padding: const EdgeInsets.symmetric(
          horizontal: 21,
          vertical: 30,
        ),
        itemBuilder: (context, index) => children[index],
        separatorBuilder: (context, index) => index > children.length - 3
            ? SizedBox(height: 10)
            : Divider(
                color: Colors.black.withOpacity(0.1),
              ),
        itemCount: children.length,
      ),
    );
  }
}
