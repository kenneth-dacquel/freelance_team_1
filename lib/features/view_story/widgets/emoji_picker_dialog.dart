import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/view_story/widgets/emoji_column.dart';
import 'package:freelance_team_1/utilities/enums.dart';
import 'package:freelance_team_1/widgets/light_custom_text_field.dart';

class EmojiPickerDialog extends StatefulWidget {
  const EmojiPickerDialog({@required this.textController});
  final TextEditingController textController;
  @override
  _EmojiPickerDialogState createState() => _EmojiPickerDialogState();
}

class _EmojiPickerDialogState extends State<EmojiPickerDialog> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      padding: const EdgeInsets.only(
        left: 20,
        top: 34,
        bottom: 20,
        right: 20,
      ),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(30.0),
          topRight: Radius.circular(30.0),
        ),
      ),
      child: SingleChildScrollView(
        child: Column(
          children: [
            LightCustomTextField(
              onTap: () {},
              withSuffix: false,
              hint: 'Search members',
              onChanged: (text) => print(text),
            ),
            SizedBox(height: 20),
            EmojiColumn(
              onEmojiTap: (emoji) => setState(() => widget.textController.text = widget.textController.text + emoji),
              type: EmojisType.RECENT,
            ),
            SizedBox(height: 30),
            EmojiColumn(
              onEmojiTap: (emoji) => setState(() => widget.textController.text = widget.textController.text + emoji),
              type: EmojisType.ALL,
            ),
          ],
        ),
      ),
    );
  }
}
