import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ReplyWidget extends StatelessWidget {
  const ReplyWidget({@required this.showReactDialog});

  final VoidCallback showReactDialog;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(width: 30),
        InkWell(
          onTap: showReactDialog,
          child: Column(
            children: [
              SvgPicture.asset(AssetImages.storyArrowUp),
              SizedBox(height: 10),
              Text(
                'Reply',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0,
                ),
              ),
            ],
          ),
        ),
        Column(
          children: [
            SvgPicture.asset(
              AssetImages.cornerUpperRight,
              height: 30,
              width: 30,
            ),
            SizedBox(height: 24),
          ],
        ),
      ],
    );
  }
}
