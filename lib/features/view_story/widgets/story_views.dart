import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class StoryViews extends StatelessWidget {
  final String count;
  final VoidCallback onTap;

  const StoryViews({
    Key key,
    this.count = '',
    this.onTap,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Row(
        children: [
          Image.asset(
            AssetImages.membersPlaceholder,
            height: 18,
            width: 35,
          ),
          SizedBox(width: 11),
          Text(
            count,
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
