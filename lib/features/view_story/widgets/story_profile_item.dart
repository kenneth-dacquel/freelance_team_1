import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class StoryProfileItem extends StatelessWidget {
  final String name;
  final String time;
  final bool hasLocation;
  final VoidCallback onEllipsisPressed;

  const StoryProfileItem({
    Key key,
    this.name,
    this.time,
    this.hasLocation = false,
    @required this.onEllipsisPressed,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Row(
      children: [
        CircleAvatar(
          child: Image.asset(
            AssetImages.member2,
            height: 40,
            width: 40,
          ),
        ),
        SizedBox(width: 10),
        Column(
          children: [
            Text(
              name,
              style: textTheme.subtitle1.copyWith(
                fontSize: 16,
                color: Colors.white,
              ),
            ),
            if (hasLocation) ...[
              SizedBox(height: 2),
              Row(
                children: [
                  SvgPicture.asset(AssetImages.mapPinIcon2),
                  Text(
                    'Bali, Indonesia',
                    style: textTheme.subtitle1.copyWith(color: Colors.white),
                  ),
                ],
              ),
            ],
          ],
        ),
        SizedBox(width: 5),
        Text(
          time,
          style: textTheme.subtitle1.copyWith(
            fontWeight: FontWeight.w500,
            color: Colors.white.withOpacity(.5),
          ),
        ),
        Spacer(),
        IconButton(
          icon: SvgPicture.asset(
            AssetImages.ellipsis,
            height: 24,
            width: 24,
            color: Colors.white,
          ),
          onPressed: onEllipsisPressed,
        ),
        IconButton(
          icon: SvgPicture.asset(
            AssetImages.closeIcon,
            height: 24,
            width: 24,
            color: Colors.white,
          ),
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );
  }
}
