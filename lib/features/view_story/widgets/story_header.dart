import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class StoryHeader extends StatelessWidget {
  const StoryHeader({
    @required this.onClosePressed,
    @required this.onMorePressed,
    this.name,
    this.location,
    this.timePassed,
  });

  final String name;
  final String location;
  final String timePassed;
  final VoidCallback onMorePressed;
  final VoidCallback onClosePressed;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 6),
      child: Row(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(30),
            child: Image.asset(AssetImages.storyUserAvatar),
          ),
          SizedBox(width: 10),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    name ?? '',
                    style: textTheme.subtitle1.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 5),
                  Text(
                    timePassed ?? '',
                    style: textTheme.subtitle1.copyWith(color: Colors.white.withOpacity(0.6)),
                  ),
                ],
              ),
              SizedBox(height: 2),
              Row(
                children: [
                  SvgPicture.asset(AssetImages.mapPinIcon2),
                  Text(
                    location ?? '',
                    style: textTheme.subtitle1.copyWith(color: Colors.white),
                  ),
                ],
              ),
            ],
          ),
          Spacer(),
          InkWell(
            onTap: onMorePressed,
            child: SvgPicture.asset(
              AssetImages.ellipsis,
              color: Colors.white,
            ),
          ),
          SizedBox(width: 26),
          InkWell(
            onTap: onClosePressed,
            child: SvgPicture.asset(
              AssetImages.closeIcon,
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }
}
