import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class EmojiColumn extends StatelessWidget {
  const EmojiColumn({
    @required this.onEmojiTap,
    this.type,
  });

  final EmojisType type;

  final Function(String) onEmojiTap;
  @override
  Widget build(BuildContext context) {
    final recentEmojis = ['👍🏻', '😂', '😧', '😍', '😍', '😧'];
    final all = ['👍🏻', '😂', '😧', '😍', '😍', '😧', '😧', '😂'];
    final isAll = type == EmojisType.ALL;
    final rows = isAll ? 4 : 2;
    final emojis = isAll ? all : recentEmojis;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          isAll ? 'All' : 'Recent',
          style: TextStyle(
            color: Colors.black,
            fontSize: 16,
            fontWeight: FontWeight.w600,
            letterSpacing: 0,
          ),
        ),
        SizedBox(height: 20),
        for (var i = 0; i < rows; i++)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ...?emojis.map(
                (emoji) => InkWell(
                  onTap: () {
                    onEmojiTap(emoji);
                    Navigator.pop(context);
                  },
                  child: Text(
                    emoji,
                    style: TextStyle(
                      fontSize: isAll ? 24 : 35,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
            ],
          ),
        SizedBox(height: 20),
      ],
    );
  }
}
