import 'package:dartx/dartx.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/create_story/widgets/add_to_highlights_dialog.dart';
import 'package:freelance_team_1/features/view_story/widgets/animated_bar.dart';
import 'package:freelance_team_1/features/view_story/widgets/reply_widget.dart';
import 'package:freelance_team_1/features/view_story/widgets/story_details_modal.dart';
import 'package:freelance_team_1/features/view_story/widgets/story_more_sheet.dart';
import 'package:freelance_team_1/features/view_story/widgets/story_profile_item.dart';
import 'package:freelance_team_1/features/view_story/widgets/story_react_dialog.dart';
import 'package:freelance_team_1/features/view_story/widgets/story_views.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/bottom_sheet_creator.dart';

class StoryView extends StatefulWidget {
  final List<Widget> stories;
  final bool isToReact;

  const StoryView({
    Key key,
    this.stories,
    this.isToReact,
  }) : super(key: key);

  @override
  _StoryViewState createState() => _StoryViewState();
}

class _StoryViewState extends State<StoryView> with SingleTickerProviderStateMixin {
  PageController pageController;
  AnimationController animationController;
  int currentIndex;

  bool highlightShown;

  @override
  void initState() {
    highlightShown = false;
    pageController = PageController();
    animationController = AnimationController(vsync: this);
    currentIndex = 0;

    final firstStory = widget.stories.first;
    loadStory(story: firstStory, animateToPage: false);

    animationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        animationController.stop();
        animationController.reset();

        setState(() {
          if (currentIndex + 1 < widget.stories.length) {
            currentIndex += 1;
            loadStory(story: widget.stories[currentIndex]);
          } else {
            Navigator.pop(context);
          }
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    pageController.dispose();
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final image = widget.stories[currentIndex];
    return GestureDetector(
      onTapDown: (details) => _onTapDown(details, image),
      child: Stack(
        children: [
          PageView.builder(
            controller: pageController,
            itemCount: widget.stories.length,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return widget.stories[index];
            },
            onPageChanged: (index) => currentIndex = index,
          ),
          Positioned(
            top: 40,
            left: 10,
            right: 10,
            child: Column(
              children: <Widget>[
                Row(
                    children: widget.stories
                        .mapIndexed(
                          (index, story) => AnimatedBar(
                            animController: animationController,
                            position: index,
                            currentIndex: currentIndex,
                          ),
                        )
                        .toList()),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15,
                    vertical: 10.0,
                  ),
                  child: StoryProfileItem(
                    name: 'Prashanth Pillai',
                    time: '3h',
                    onEllipsisPressed: () => _createBottomSheetDialog(StoryMoreSheet(isHighlight: widget.isToReact)),
                    hasLocation: widget.isToReact,
                  ),
                ),
              ],
            ),
          ),
          if (animationController.isAnimating)
            widget.isToReact
                ? Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      children: [
                        Spacer(),
                        ReplyWidget(showReactDialog: showReactDialog),
                        SizedBox(height: 70),
                      ],
                    ),
                  )
                : Positioned(
                    bottom: 50,
                    left: 20,
                    right: 20,
                    child: Row(
                      children: [
                        StoryViews(
                          count: '123',
                          onTap: () => _createBottomSheetDialog(
                            StoryDetailsModal(showHighlightsDialog),
                            after: () {
                              if (highlightShown == false) {
                                animationController.forward();
                                setState(() {});
                              }
                            },
                          ),
                        ),
                        Spacer(),
                        IconButton(
                          icon: SvgPicture.asset(
                            AssetImages.plusCircle,
                            height: 24,
                            width: 24,
                          ),
                          onPressed: () {},
                        ),
                        IconButton(
                          icon: SvgPicture.asset(
                            AssetImages.sendIcon,
                            height: 24,
                            width: 24,
                          ),
                          onPressed: () {},
                        ),
                      ],
                    ),
                  ),
        ],
      ),
    );
  }

  void _createBottomSheetDialog(Widget dialog, {VoidCallback before, VoidCallback after, Color barrierColor}) {
    BottomSheetCreator(
      context: context,
      dialog: dialog,
      barrierColor: barrierColor,
      before: () {
        if (before != null) {
          before();
        } else {
          animationController.stop(canceled: false);
          setState(() {});
        }
      },
      after: () {
        if (after != null) {
          after();
        } else {
          animationController.forward();
          setState(() {});
        }
      },
    ).showBottomSheet();
  }

  void showHighlightsDialog() {
    _createBottomSheetDialog(
      AddToHighlightsDialog(),
      before: () {
        Navigator.pop(context);
        setState(() {
          highlightShown = true;
        });
        animationController.stop(canceled: false);
      },
      after: () {
        animationController.forward();
        setState(() {
          highlightShown = false;
        });
      },
    );
  }

  void showMoreDialog() {
    animationController.stop(canceled: false);
    setState(() {});
    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      context: context,
      builder: (_) => StoryMoreSheet(isHighlight: widget.isToReact),
    ).then((_) {
      animationController.forward();
      setState(() {});
    });
  }

  void showReactDialog() {
    animationController.stop(canceled: false);
    setState(() {});
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.black.withOpacity(0.8),
      isScrollControlled: true,
      builder: (_) => StoryReactDialog(
        stopAnimation: () {
          animationController.stop(canceled: false);
          setState(() {});
        },
        continueAnimation: () {
          animationController.forward();
          setState(() {});
        },
      ),
    ).then((_) {
      animationController.forward();
      setState(() {});
    });
  }

  void _onTapDown(TapDownDetails details, Widget story) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double dx = details.globalPosition.dx;
    if (dx < screenWidth / 3) {
      setState(() {
        if (currentIndex - 1 >= 0) {
          currentIndex -= 1;
          loadStory(story: widget.stories[currentIndex]);
        }
      });
    } else if (dx > 2 * screenWidth / 3) {
      setState(() {
        if (currentIndex + 1 < widget.stories.length) {
          currentIndex += 1;
          loadStory(story: widget.stories[currentIndex]);
        } else {
          Navigator.pop(context);
        }
      });
    }
  }

  void loadStory({Widget story, bool animateToPage = true}) {
    animationController.stop();
    animationController.reset();

    animationController.duration = Duration(seconds: 5);
    animationController.forward();

    if (animateToPage) {
      pageController.animateToPage(
        currentIndex,
        duration: const Duration(milliseconds: 1),
        curve: Curves.easeInOut,
      );
    }
  }
}
