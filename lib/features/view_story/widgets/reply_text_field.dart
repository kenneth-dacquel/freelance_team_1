import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class ReplyTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final emojis = ['👍🏻', '😂', '😧', '😍'];
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ...?emojis.map(
              (emoji) => InkWell(
                onTap: () => print(emoji),
                child: Text(
                  emoji,
                  style: TextStyle(fontSize: 30),
                ),
              ),
            ),
            InkWell(
              onTap: () => print('TEST'),
              child: SvgPicture.asset(
                AssetImages.plusIcon,
                width: 25,
                height: 25,
                color: Colors.white,
              ),
            ),
            SizedBox(),
            InkWell(
              onTap: () => print('TEST REPLY'),
              child: SvgPicture.asset(
                AssetImages.cornerUpperRight,
                width: 25,
                height: 25,
                color: Colors.white,
              ),
            ),
          ],
        ),
        SizedBox(height: 25),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () => print('TEST'),
              child: Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.white.withOpacity(0.5)),
                ),
                child: SvgPicture.asset(
                  AssetImages.plusIcon,
                  color: Colors.white,
                  height: 20,
                  width: 20,
                  fit: BoxFit.scaleDown,
                ),
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: TextFormField(
                decoration: InputDecoration(
                  suffixIcon: Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: SvgPicture.asset(AssetImages.micIcon),
                  ),
                  suffixIconConstraints: BoxConstraints(
                    maxHeight: 50,
                    maxWidth: 50,
                  ),
                  hintText: 'Send Message',
                  hintStyle: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    letterSpacing: 0,
                    color: Colors.white.withOpacity(0.6),
                  ),
                  contentPadding: const EdgeInsets.symmetric(
                    horizontal: 18,
                    vertical: 10,
                  ),
                  filled: true,
                  fillColor: Colors.black,
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(52)),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(52),
                    borderSide: BorderSide(color: Colors.white.withOpacity(0.5)),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(52),
                    borderSide: BorderSide(color: Colors.white.withOpacity(0.5)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
