import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/view_story/widgets/emoji_picker_dialog.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class StoryReactDialog extends StatefulWidget {
  const StoryReactDialog({
    @required this.stopAnimation,
    @required this.continueAnimation,
  });

  final VoidCallback stopAnimation;
  final VoidCallback continueAnimation;

  @override
  _StoryReactDialogState createState() => _StoryReactDialogState();
}

class _StoryReactDialogState extends State<StoryReactDialog> {
  TextEditingController controller;

  @override
  void initState() {
    controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void showEmojiDialog() {
    widget.stopAnimation();
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      barrierColor: Colors.transparent,
      isScrollControlled: true,
      builder: (_) => EmojiPickerDialog(
        textController: controller,
      ),
    ).then((_) {
      widget.continueAnimation();
    });
  }

  @override
  Widget build(BuildContext context) {
    final emojis = ['👍🏻', '😂', '😧', '😍'];
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        resizeToAvoidBottomInset: true,
        body: Container(
          padding: EdgeInsets.zero,
          color: Colors.transparent,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ...?emojis.map(
                      (emoji) => InkWell(
                        onTap: () => setState(() {
                          controller.text = controller.text + emoji;
                          controller.selection =
                              TextSelection.fromPosition(TextPosition(offset: controller.text.length));
                        }),
                        child: Text(
                          emoji,
                          style: TextStyle(fontSize: 30),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: showEmojiDialog,
                      child: SvgPicture.asset(
                        AssetImages.plusIcon,
                        width: 25,
                        height: 25,
                        color: Colors.white,
                      ),
                    ),
                    SizedBox(),
                    InkWell(
                      onTap: () {
                        print(controller.text);
                        Navigator.pop(context);
                      },
                      child: SvgPicture.asset(
                        AssetImages.cornerUpperRight,
                        width: 25,
                        height: 25,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 25),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                      onTap: () => print('TEST'),
                      child: Container(
                        height: 36,
                        width: 36,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white.withOpacity(0.5)),
                        ),
                        child: SvgPicture.asset(
                          AssetImages.plusIcon,
                          color: Colors.white,
                          height: 20,
                          width: 20,
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: TextFormField(
                        controller: controller,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                          suffixIcon: Padding(
                            padding: const EdgeInsets.only(right: 20),
                            child: SvgPicture.asset(AssetImages.micIcon),
                          ),
                          suffixIconConstraints: BoxConstraints(
                            maxHeight: 50,
                            maxWidth: 50,
                          ),
                          hintText: 'Send Message',
                          hintStyle: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 0,
                            color: Colors.white.withOpacity(0.6),
                          ),
                          contentPadding: const EdgeInsets.symmetric(
                            horizontal: 18,
                            vertical: 10,
                          ),
                          filled: true,
                          fillColor: Colors.black,
                          border: OutlineInputBorder(borderRadius: BorderRadius.circular(52)),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(52),
                            borderSide: BorderSide(color: Colors.white.withOpacity(0.5)),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(52),
                            borderSide: BorderSide(color: Colors.white.withOpacity(0.5)),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 35),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
