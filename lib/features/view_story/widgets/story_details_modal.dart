import 'package:fading_edge_scrollview/fading_edge_scrollview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/view_story/model/story_viewer_model.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:scroll_snap_list/scroll_snap_list.dart';

class StoryDetailsModal extends StatefulWidget {
  const StoryDetailsModal(this.showHighlights);

  final VoidCallback showHighlights;
  @override
  _StoryDetailsModalState createState() => _StoryDetailsModalState();
}

class _StoryDetailsModalState extends State<StoryDetailsModal> {
  final controller = ScrollController();
  int focusIndex = 0;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Material(
      color: Colors.transparent,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          children: [
            SizedBox(height: 80),
            Text(
              'Reactions',
              textAlign: TextAlign.center,
              style: textTheme.button.copyWith(color: Colors.white),
            ),
            SizedBox(height: 40),
            GestureDetector(
              onHorizontalDragDown: (_) => Navigator.pop(context),
              child: SvgPicture.asset(
                AssetImages.storyArrowDown,
                height: 16,
                width: 49,
              ),
            ),
            SizedBox(height: 30),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                '430 Viewers',
                style: textTheme.headline5.copyWith(color: Colors.white),
              ),
            ),
            Expanded(
              child: FadingEdgeScrollView.fromScrollView(
                child: ListView.builder(
                  controller: controller,
                  itemCount: StoryViewerModel.sampleData().length,
                  itemBuilder: (context, index) {
                    final viewer = StoryViewerModel.sampleData()[index];
                    return ListTile(
                      title: Row(
                        children: [
                          Text(viewer.name,
                              style: textTheme.subtitle1.copyWith(
                                fontWeight: FontWeight.w600,
                                color: Colors.white,
                              )),
                          SizedBox(width: 10),
                          if (viewer.verified)
                            SvgPicture.asset(
                              AssetImages.verifiedIcon,
                              height: 14,
                              width: 14,
                            )
                        ],
                      ),
                      subtitle: Text(
                        viewer.username,
                        style: TextStyle(color: Colors.white.withOpacity(0.5)),
                      ),
                      leading: Image.asset(viewer.imagePath),
                      trailing: Text(
                        viewer.react,
                        style: TextStyle(fontSize: 20),
                      ),
                    );
                  },
                ),
              ),
            ),
            Expanded(
              child: ScrollSnapList(
                itemCount: 10,
                scrollDirection: Axis.horizontal,
                itemSize: 66,
                focusOnItemTap: true,
                updateOnScroll: true,
                onItemFocus: (index) {
                  setState(() {
                    focusIndex = index;
                  });
                },
                itemBuilder: (context, index) {
                  return UnconstrainedBox(
                    child: Stack(
                      children: [
                        Container(
                          width: 56,
                          height: 100,
                          margin: const EdgeInsets.symmetric(horizontal: 5),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: focusIndex == index
                                ? Border.all(
                                    color: Colors.white,
                                    width: 2,
                                  )
                                : null,
                          ),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Image.asset(
                              AssetImages.storyBackgroundImage,
                              fit: BoxFit.fitWidth,
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 5,
                          left: 0,
                          right: 0,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                AssetImages.eyeIcon,
                                height: 11.37,
                                width: 11.37,
                              ),
                              SizedBox(width: 3),
                              Text(
                                '420',
                                style: textTheme.bodyText1.copyWith(color: Colors.white),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
            Row(
              children: [
                IconButton(
                  icon: SvgPicture.asset(
                    AssetImages.deleteIconClear,
                  ),
                  onPressed: () {},
                ),
                IconButton(
                  icon: SvgPicture.asset(
                    AssetImages.downloadIconClear,
                    width: 24,
                    height: 24,
                  ),
                  onPressed: () {},
                ),
                Spacer(),
                IconButton(
                  icon: SvgPicture.asset(
                    AssetImages.plusCircle,
                    width: 24,
                    height: 24,
                  ),
                  onPressed: widget.showHighlights,
                ),
                IconButton(
                  icon: SvgPicture.asset(
                    AssetImages.sendIcon,
                    width: 24,
                    height: 24,
                  ),
                  onPressed: () {},
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
