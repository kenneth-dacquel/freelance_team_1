import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/story_settings/models/replier.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class StoryReplies extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StoryRepliesState();
}

class _StoryRepliesState extends State<StoryReplies> {
  final options = [
    Replier(
      category: 'All',
      type: AllowReplies.ALL,
    ),
    Replier(
      category: 'Your contacts',
      type: AllowReplies.CONTACTS,
    ),
    Replier(
      category: 'Friends',
      type: AllowReplies.FRIENDS,
    ),
  ];

  bool isAllowed;
  AllowReplies replies;

  @override
  void initState() {
    isAllowed = false;
    replies = AllowReplies.ALL;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Allow message replies',
              style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.w600,
                letterSpacing: 0,
              ),
              textAlign: TextAlign.start,
            ),
            Switch(
              activeColor: Colors.white,
              activeTrackColor: Colors.green,
              value: isAllowed,
              onChanged: (newValue) => setState(() => isAllowed = newValue),
            ),
          ],
        ),
        SizedBox(height: 26),
        ...?options.map((option) => ListTile(
              contentPadding: EdgeInsets.zero,
              title: Text(
                option.category,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0,
                ),
              ),
              subtitle: Text(
                option.subtitle,
                style: TextStyle(
                  fontSize: 12,
                  color: vulcan.withOpacity(0.5),
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0,
                ),
              ),
              trailing: InkWell(
                child: Container(
                  padding: const EdgeInsets.only(right: 10),
                  child: SvgPicture.asset(
                    option.type == replies ? AssetImages.activeCheckbox : AssetImages.inactiveCheckbox,
                    height: 22,
                    width: 22,
                  ),
                ),
                onTap: () => setState(() => replies = option.type),
              ),
            )),
        SizedBox(height: 50),
      ],
    );
  }
}
