import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/constants.dart';

class StoryExceptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Text(
          'Exceptions',
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w600,
            letterSpacing: 0,
          ),
          textAlign: TextAlign.start,
        ),
        SizedBox(height: 26),
        InkWell(
          onTap: () => print('Exceptions'),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Hide story from',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 0,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(right: 10),
                    child: Row(
                      children: [
                        Text(
                          '1 user',
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: vulcan.withOpacity(0.5),
                            letterSpacing: 0,
                          ),
                        ),
                        SizedBox(width: 15),
                        SvgPicture.asset(
                          AssetImages.rightArrowIcon2,
                          height: 10,
                          width: 10,
                          color: Colors.black,
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Text(
                description,
                style: TextStyle(
                  fontSize: 12,
                  color: vulcan.withOpacity(0.5),
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0,
                ),
              ),
            ],
          ),
        ),
        SizedBox(height: 33),
      ],
    );
  }
}
