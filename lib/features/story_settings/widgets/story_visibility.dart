import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/story_settings/models/viewer.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class StoryVisibility extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _StoryVisibilityState();
}

class _StoryVisibilityState extends State<StoryVisibility> {
  StoryViewer viewer;

  @override
  void initState() {
    viewer = StoryViewer.ALL;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final options = [
      Viewer(
        category: 'All',
        type: StoryViewer.ALL,
      ),
      Viewer(
        category: 'My contacts',
        type: StoryViewer.CONTACTS,
      ),
      Viewer(
        category: 'Friends',
        type: StoryViewer.FRIENDS,
      ),
      Viewer(
        category: 'Specific location',
        type: StoryViewer.LOCATION,
      ),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Who can see your story?',
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w600,
            letterSpacing: 0,
          ),
          textAlign: TextAlign.start,
        ),
        SizedBox(height: 15),
        ...?options.map((option) => ListTile(
              contentPadding: EdgeInsets.zero,
              title: Text(
                option.category,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0,
                ),
              ),
              subtitle: Text(
                option.subtitle,
                style: TextStyle(
                  fontSize: 12,
                  color: vulcan.withOpacity(0.5),
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0,
                ),
              ),
              trailing: InkWell(
                child: Container(
                  padding: const EdgeInsets.only(right: 10),
                  child: SvgPicture.asset(
                    option.type == viewer ? AssetImages.activeCheckbox : AssetImages.inactiveCheckbox,
                    height: 22,
                    width: 22,
                  ),
                ),
                onTap: () => setState(() => viewer = option.type),
              ),
            )),
        SizedBox(height: 15),
      ],
    );
  }
}
