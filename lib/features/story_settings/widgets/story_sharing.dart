import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/story_settings/models/sharer.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/constants.dart';

class StorySharing extends StatefulWidget {
  StorySharing(this.options);
  final List<Sharer> options;

  @override
  _StorySharingState createState() => _StorySharingState();
}

class _StorySharingState extends State<StorySharing> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 30),
        Text(
          'Sharing',
          style: TextStyle(
            color: Colors.black,
            fontSize: 18,
            fontWeight: FontWeight.w600,
            letterSpacing: 0,
          ),
          textAlign: TextAlign.start,
        ),
        SizedBox(height: 26),
        ...?widget.options.map((option) => ListTile(
              contentPadding: EdgeInsets.zero,
              title: Text(
                option.title,
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.black,
                  fontWeight: FontWeight.w500,
                  letterSpacing: 0,
                ),
              ),
              subtitle: Text(
                description,
                style: TextStyle(
                  fontSize: 12,
                  color: vulcan.withOpacity(0.5),
                  fontWeight: FontWeight.w400,
                  letterSpacing: 0,
                ),
              ),
              trailing: Switch(
                activeColor: Colors.white,
                activeTrackColor: Colors.green,
                value: option.isActive,
                onChanged: (newValue) => setState(() => option.isActive = newValue),
              ),
            )),
        SizedBox(height: 15),
      ],
    );
  }
}
