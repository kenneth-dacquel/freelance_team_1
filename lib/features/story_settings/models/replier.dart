import 'package:freelance_team_1/utilities/constants.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class Replier {
  Replier({
    this.category,
    this.subtitle = description,
    this.type = AllowReplies.ALL,
  });

  String category;
  String subtitle;
  AllowReplies type;
}
