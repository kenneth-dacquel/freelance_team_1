import 'package:freelance_team_1/utilities/constants.dart';

class Sharer {
  Sharer({
    this.title,
    this.subtitle = description,
    this.isActive = false,
  });

  String title;
  String subtitle;
  bool isActive;
}
