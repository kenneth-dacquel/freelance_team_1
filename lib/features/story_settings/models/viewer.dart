import 'package:freelance_team_1/utilities/enums.dart';

class Viewer {
  Viewer({
    this.category,
    this.subtitle = 'Description goes here',
    this.type,
  });

  String category;
  String subtitle;
  StoryViewer type;
}
