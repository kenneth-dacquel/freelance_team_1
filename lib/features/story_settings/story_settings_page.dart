import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/story_settings/models/sharer.dart';
import 'package:freelance_team_1/features/story_settings/widgets/story_exceptions.dart';
import 'package:freelance_team_1/features/story_settings/widgets/story_replies.dart';
import 'package:freelance_team_1/features/story_settings/widgets/story_sharing.dart';
import 'package:freelance_team_1/features/story_settings/widgets/story_visibility.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';

class StorySettingsPage extends StatefulWidget {
  static const String route = 'story-settings-page';
  @override
  State<StatefulWidget> createState() => _StorySettingsPageState();
}

class _StorySettingsPageState extends State<StorySettingsPage> {
  final options = [
    Sharer(
      title: 'Allow sharing as message',
      isActive: true,
    ),
    Sharer(
      title: 'Allow sharing as story',
      isActive: true,
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Story settings',
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.w700,
            letterSpacing: 0,
            color: Colors.black,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: AppBarButton(
              text: 'Done',
              onPressed: () => Navigator.pop(context),
              textColor: Colors.black,
            ),
          ),
        ],
      ),
      body: ListView.separated(
        padding: const EdgeInsets.only(
          left: 25,
          right: 29,
          top: 25,
        ),
        itemBuilder: (context, index) {
          if (index == 0) {
            return StoryVisibility();
          } else if (index == 1) {
            return StoryExceptions();
          } else if (index == 2) {
            return StorySharing(options);
          } else {
            return StoryReplies();
          }
        },
        separatorBuilder: (context, index) => Container(
          height: 1,
          color: vulcan.withOpacity(0.1),
        ),
        itemCount: 4,
      ),
    );
  }
}
