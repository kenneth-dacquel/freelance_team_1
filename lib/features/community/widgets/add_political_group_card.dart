import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_button.dart';
import 'package:freelance_team_1/widgets/app_container.dart';

class AddPoliticalGroupCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return AppContainer(
      height: 165,
      padding: const EdgeInsets.all(5),
      margin: const EdgeInsets.symmetric(horizontal: 27),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            flex: 3,
            child: Row(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      'Title goes here',
                      style: textTheme.headline5,
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    height: double.infinity,
                    child: Text(
                      'Illustration',
                      style: textTheme.headline5.copyWith(color: Colors.black.withOpacity(.6)),
                    ),
                    decoration: BoxDecoration(color: peach, borderRadius: BorderRadius.circular(3)),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 11),
          AppButton(
            onPressed: () {},
            label: 'Add Political Group',
            backgroundColor: green.withOpacity(.1),
            labelColor: darkGreen,
          )
        ],
      ),
    );
  }
}
