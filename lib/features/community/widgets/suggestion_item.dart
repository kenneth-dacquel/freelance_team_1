import 'package:flutter/material.dart';
import 'package:freelance_team_1/widgets/app_container.dart';

class SuggestionItem extends StatelessWidget {
  final String label;

  const SuggestionItem({
    Key key,
    this.label,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return AppContainer(
      width: 150,
      padding: const EdgeInsets.fromLTRB(16, 14, 16, 18),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _SuggestionIconPlaceholder(),
          SizedBox(height: 12),
          Text(
            label,
            style: textTheme.headline4,
          ),
        ],
      ),
    );
  }
}

class _SuggestionIconPlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      width: 36,
      decoration: BoxDecoration(
        color: Color(0xFFA4CFF6),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }
}
