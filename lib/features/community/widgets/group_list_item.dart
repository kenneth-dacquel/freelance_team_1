import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/community/models/political_group.dart';
import 'package:freelance_team_1/features/group_details/joined_group_details.dart';
import 'package:freelance_team_1/features/group_details/unjoined_group_details.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_container.dart';

class GroupListItem extends StatelessWidget {
  final Group group;

  const GroupListItem({
    Key key,
    this.group,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return UnconstrainedBox(
      child: AppContainer(
        width: 330,
        padding: const EdgeInsets.all(13),
        margin: const EdgeInsets.only(right: 15),
        onTap: () => Navigator.pushNamed(context, UnjoinedGroupDetails.route),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Image.asset(
              AssetImages.groupLogo,
              height: 64,
              width: 64,
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    group.name,
                    style: textTheme.button,
                  ),
                  SizedBox(height: 10),
                  Row(
                    children: [
                      Image.asset(
                        AssetImages.memberIcons,
                        width: 40,
                        height: 27,
                      ),
                      SizedBox(width: 6),
                      Expanded(
                        child: Text(
                          group.members,
                          style: textTheme.caption,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            FlatButton(
              minWidth: 64,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(6),
                side: BorderSide(
                  color: Colors.black.withOpacity(.10),
                ),
              ),
              onPressed: () => Navigator.pushNamed(context, JoinedGroupDetails.route),
              child: Text(
                'Join',
                style: textTheme.button.copyWith(color: darkGreen),
              ),
            )
          ],
        ),
      ),
    );
  }
}
