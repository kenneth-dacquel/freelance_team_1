import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class FilterChips extends StatefulWidget {
  final List<String> titles;
  final Function(int) onChipSelected;
  final int selectedIndex;

  const FilterChips({
    Key key,
    this.titles,
    this.onChipSelected,
    this.selectedIndex,
  }) : super(key: key);

  @override
  _FilterChipsState createState() => _FilterChipsState();
}

class _FilterChipsState extends State<FilterChips> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: widget.titles.length,
        itemBuilder: (context, index) {
          return _Chip(
            key: ValueKey(widget.titles[index]),
            title: widget.titles[index],
            isSelected: widget.selectedIndex == index,
            onTap: () => widget.onChipSelected(index),
          );
        },
      ),
    );
  }
}

class _Chip extends StatelessWidget {
  final String title;
  final bool isSelected;
  final VoidCallback onTap;

  const _Chip({
    Key key,
    this.title,
    this.isSelected,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: isSelected ? selectedChipBackgroundColor : unselectedChipBackgroundColor,
        ),
        padding: const EdgeInsets.symmetric(
          horizontal: 30,
          vertical: 12,
        ),
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: Text(
          title,
          style: textTheme.bodyText2.copyWith(
            color: isSelected ? selectedChipTextColor.withOpacity(0.8) : unselectedChipTextColor,
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}
