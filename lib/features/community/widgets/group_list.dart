import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/community/models/political_group.dart';
import 'package:freelance_team_1/features/community/widgets/group_list_item.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class GroupList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
        children: PoliticalGroup.sampleData().map(
      (e) {
        return Column(
          children: [
            _GroupListHeader(politicalGroup: e),
            SizedBox(
              height: 150,
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 23),
                scrollDirection: Axis.horizontal,
                itemCount: e.groups.length,
                itemBuilder: (context, index) => GroupListItem(
                  group: e.groups[index],
                ),
              ),
            )
          ],
        );
      },
    ).toList());
  }
}

class _GroupListHeader extends StatelessWidget {
  final PoliticalGroup politicalGroup;

  const _GroupListHeader({
    Key key,
    this.politicalGroup,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 23),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                politicalGroup.typeToString.toUpperCase(),
                style: textTheme.bodyText1.copyWith(color: politicalGroup.typeColor),
              ),
              Text(
                politicalGroup.location,
                style: textTheme.headline4,
              ),
            ],
          ),
          InkWell(
            onTap: () {},
            child: Text(
              'See all',
              style: textTheme.button.copyWith(color: darkGreen),
            ),
          )
        ],
      ),
    );
  }
}
