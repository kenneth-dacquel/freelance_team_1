import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/add_group/add_group_page.dart';
import 'package:freelance_team_1/features/community/community_details_page.dart';
import 'package:freelance_team_1/features/community/widgets/suggestion_item.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/app_container.dart';
import 'package:freelance_team_1/widgets/pravasi_app_bar.dart';

class CommunityPage extends StatelessWidget {
  static const route = 'community_page';
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: PravasiAppBar(
        title: 'Community',
        leading: IconButton(
          icon: SvgPicture.asset(
            AssetImages.backButtonIcon,
            width: 9,
            height: 18,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          IconButton(
            padding: const EdgeInsets.only(right: 10),
            icon: SvgPicture.asset(
              AssetImages.search,
              height: 24,
              width: 24,
            ),
            onPressed: () {},
          )
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 25),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 33),
            AppContainer(
              onTap: () => Navigator.pushNamed(context, CommunityDetailsPage.route),
              padding: const EdgeInsets.fromLTRB(18, 15, 0, 15),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'DISTRICT',
                          style: textTheme.bodyText2.copyWith(color: gold),
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Thrissur',
                          style: textTheme.headline4,
                        ),
                        SizedBox(height: 5),
                        Text(
                          'Local groups, jobs, events and more',
                          style: textTheme.caption,
                        ),
                      ],
                    ),
                  ),
                  IconButton(
                    icon: SvgPicture.asset(
                      AssetImages.forward,
                      width: 10,
                      height: 10,
                    ),
                    onPressed: () {},
                  )
                ],
              ),
            ),
            SizedBox(height: 41),
            Text(
              'Suggestions',
              style: textTheme.headline4,
            ),
            SizedBox(height: 22),
            Wrap(
              spacing: 19,
              runSpacing: 26,
              children: ['Groups', 'Pages', 'Jobs'].map((e) {
                return InkWell(
                  onTap: () => e == 'Groups' ? Navigator.pushNamed(context, AddGroupPage.route) : null,
                  child: SuggestionItem(label: e),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }
}
