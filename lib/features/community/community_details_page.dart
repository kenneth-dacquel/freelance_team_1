import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/community/widgets/add_political_group_card.dart';
import 'package:freelance_team_1/features/community/widgets/filter_chips.dart';
import 'package:freelance_team_1/features/community/widgets/group_list.dart';
import 'package:freelance_team_1/features/view_story/view_stories_page.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/widgets/pravasi_app_bar.dart';

class CommunityDetailsPage extends StatefulWidget {
  static const route = 'community_details_page';

  @override
  _CommunityDetailsPageState createState() => _CommunityDetailsPageState();
}

class _CommunityDetailsPageState extends State<CommunityDetailsPage> {
  int selectedFilter;

  @override
  void initState() {
    selectedFilter = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Scaffold(
      appBar: PravasiAppBar(
        title: 'Thrissur',
        leading: IconButton(
          icon: SvgPicture.asset(
            AssetImages.backButtonIcon,
            width: 9,
            height: 18,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          IconButton(
            icon: SvgPicture.asset(
              AssetImages.bell,
              width: 24,
              height: 24,
            ),
            onPressed: () {},
          ),
          IconButton(
            icon: SvgPicture.asset(
              AssetImages.plus,
              width: 20,
              height: 20,
            ),
            onPressed: () => Navigator.pushNamed(context, ViewStoriesPage.route),
          )
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(child: SizedBox(height: 21)),
          SliverFillRemaining(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  FilterChips(
                    titles: [
                      'Politics',
                      'Groups',
                      'Jobs',
                      'Events',
                    ],
                    selectedIndex: selectedFilter,
                    onChipSelected: (index) => setState(() => selectedFilter = index),
                  ),
                  SizedBox(height: 40),
                  Text(
                    'Politics related heading goes here',
                    style: textTheme.headline4,
                  ),
                  SizedBox(height: 50),
                  AddPoliticalGroupCard(),
                  SizedBox(height: 40),
                  GroupList(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
