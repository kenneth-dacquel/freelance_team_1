import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

enum GroupType { District, Municipality, Ward }

class PoliticalGroup {
  final GroupType type;
  final String location;
  final List<Group> groups;

  PoliticalGroup({this.type, this.location, this.groups});

  Color get typeColor {
    switch (type) {
      case GroupType.District:
        return red;
        break;
      case GroupType.Municipality:
        return green;
        break;
      case GroupType.Ward:
        return blue;
        break;
      default:
        return Colors.black;
    }
  }

  String get typeToString => type.toString().split('.').last;

  static List<PoliticalGroup> sampleData() {
    return [
      PoliticalGroup(
        type: GroupType.District,
        location: 'Thrissur',
        groups: [
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
        ],
      ),
      PoliticalGroup(
        type: GroupType.Municipality,
        location: 'Chalakkudy',
        groups: [
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
        ],
      ),
      PoliticalGroup(
        type: GroupType.Ward,
        location: 'Chuloor',
        groups: [
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
          Group(
            name: 'CPIM Thrissur',
            members: '8.2k members',
          ),
        ],
      ),
    ];
  }
}

class Group {
  final String name;
  final String members;

  Group({this.name, this.members});
}
