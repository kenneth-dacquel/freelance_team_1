import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/about_group/widgets/about_header.dart';
import 'package:freelance_team_1/features/about_group/widgets/about_info.dart';
import 'package:freelance_team_1/features/about_group/widgets/about_list.dart';
import 'package:freelance_team_1/features/about_group/widgets/group_admin_rules.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class AboutGroupPage extends StatelessWidget {
  static const String route = 'about-group-page';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset(AssetImages.closeIcon),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          'About',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            AboutHeader(),
            SizedBox(height: 20),
            Container(
              height: 1,
              width: MediaQuery.of(context).size.width,
              color: divider,
            ),
            SizedBox(height: 20),
            Container(
              color: Colors.transparent,
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AboutInfo(),
                  SizedBox(height: 20),
                  GroupAdminRules(),
                  SizedBox(height: 20),
                  AboutList(),
                  SizedBox(height: 70),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
