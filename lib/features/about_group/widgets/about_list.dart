import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class AboutList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            'Date created',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
          trailing: Text(
            '20 Aug 2018',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: vulcan.withOpacity(0.5),
            ),
          ),
        ),
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            'Group Based',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
          trailing: Text(
            'London, United Kingdom',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: vulcan.withOpacity(0.5),
            ),
          ),
        ),
        ListTile(
          contentPadding: EdgeInsets.zero,
          title: Text(
            'Group type',
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.w500,
            ),
          ),
          trailing: Text(
            'General',
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: vulcan.withOpacity(0.5),
            ),
          ),
        ),
        InkWell(
          onTap: null,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Group history',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            trailing: SvgPicture.asset(
              AssetImages.rightArrowIcon2,
              height: 12,
              width: 5,
              color: Colors.black,
            ),
          ),
        ),
        InkWell(
          onTap: null,
          child: ListTile(
            contentPadding: EdgeInsets.zero,
            title: Text(
              'Group by',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
            trailing: SizedBox(
              width: 65,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset(
                    AssetImages.memberIcons,
                    width: 40,
                    height: 27,
                  ),
                  SvgPicture.asset(
                    AssetImages.rightArrowIcon2,
                    height: 12,
                    width: 5,
                    color: Colors.black,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
