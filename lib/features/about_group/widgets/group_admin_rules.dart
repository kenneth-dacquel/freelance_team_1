import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/constants.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GroupAdminRules extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 15,
      margin: EdgeInsets.zero,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: ListTile(
        title: Text(
          'Group rules from the admins',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
        subtitle: Text(
          description,
          style: TextStyle(
            fontSize: 14,
            color: vulcan.withOpacity(0.5),
          ),
        ),
        trailing: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 12),
            SvgPicture.asset(
              AssetImages.rightArrowIcon2,
              height: 12,
              width: 5,
              color: Colors.black,
            ),
          ],
        ),
      ),
    );
  }
}
