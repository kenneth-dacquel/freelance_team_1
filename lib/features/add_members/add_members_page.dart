import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/add_members/models/member.dart';
import 'package:freelance_team_1/features/add_members/widgets/member_chip.dart';
import 'package:freelance_team_1/features/add_members/widgets/members_section.dart';
import 'package:freelance_team_1/features/create_group/create_group_page.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/enums.dart';
import 'package:freelance_team_1/widgets/app_bar_button.dart';
import 'package:freelance_team_1/widgets/light_custom_text_field.dart';

class AddMembersPage extends StatefulWidget {
  static const String route = 'add-members-page';
  @override
  State<StatefulWidget> createState() => _AddMembersPageState();
}

class _AddMembersPageState extends State<AddMembersPage> {
  final members = [
    Member(
      name: 'Vishnu Xavier',
      subtitle: 'number',
      image: AssetImages.member2,
      isJoined: true,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Muhammad Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: true,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Jay Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: true,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince Xavier',
      subtitle: 'number',
      image: AssetImages.member1,
      isJoined: false,
      isContact: true,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince Vinayan',
      subtitle: 'number',
      image: AssetImages.member2,
      isJoined: false,
      isContact: true,
      isCertified: true,
      isChecked: true,
    ),
    Member(
      name: 'Blex Aolton',
      subtitle: 'username',
      image: AssetImages.member3,
      isJoined: false,
      isContact: false,
      isCertified: false,
      isChecked: false,
    ),
    Member(
      name: 'Prince',
      subtitle: 'username',
      image: AssetImages.member4,
      isJoined: false,
      isContact: false,
      isCertified: false,
      isChecked: false,
    ),
  ];

  List<Member> joinedMembers;
  List<Member> filteredMembers;

  @override
  void initState() {
    joinedMembers = members.where((element) => element.isJoined).toList();
    filteredMembers = members.where((element) => element.isJoined == false).toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: AppBarButton(
          text: 'Cancel',
          textColor: Colors.black,
          onPressed: () => Navigator.pop(context),
        ),
        leadingWidth: 80,
        title: Column(
          children: [
            Text(
              'Add Members',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
                letterSpacing: 0,
              ),
            ),
            Text(
              '1/number',
              style: TextStyle(
                color: vulcan.withOpacity(0.5),
                fontSize: 12,
                letterSpacing: 0,
              ),
            ),
          ],
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5),
            child: AppBarButton(
              text: 'Next',
              textColor: darkGreen,
              onPressed: () => Navigator.pushNamed(context, CreateGroupPage.route),
            ),
          ),
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 25),
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 25),
                      child: LightCustomTextField(
                        onTap: () {},
                        isFocused: true,
                        hint: 'Search members',
                        onChanged: (text) {
                          final filtered = members
                              .where((element) =>
                                  element.name.toLowerCase().contains(text.toLowerCase()) && element.isJoined == false)
                              .toList();
                          setState(() {
                            filteredMembers = filtered;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: 15),
                    if (joinedMembers.isNotEmpty) ...[
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 25),
                        height: 40,
                        child: ListView.separated(
                          separatorBuilder: (c, index) => SizedBox(width: 13),
                          itemBuilder: (c, index) {
                            return MemberChip(
                              onTap: () {
                                setState(() {
                                  final member = joinedMembers[index];
                                  members.firstWhere((element) => element.name == member.name).isJoined = false;
                                  member.isJoined = false;
                                  joinedMembers.removeAt(index);
                                  filteredMembers.add(member);
                                });
                              },
                              member: joinedMembers[index],
                            );
                          },
                          itemCount: joinedMembers.length,
                          scrollDirection: Axis.horizontal,
                        ),
                      ),
                      SizedBox(height: 15),
                    ],
                    MembersSection(
                      type: MemberType.CONTACT,
                      members: filteredMembers.where((element) => element.isContact).toList(),
                    ),
                    SizedBox(height: 5),
                    MembersSection(
                      type: MemberType.FOLLOWING,
                      members: filteredMembers.where((element) => element.isContact == false).toList(),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
