import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/add_members/models/member.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class MemberChip extends StatelessWidget {
  const MemberChip({
    @required this.onTap,
    this.member,
  });

  final Member member;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(40),
        border: Border.all(
          color: lightGrey.withOpacity(0.1),
          width: 2,
        ),
      ),
      child: Row(
        children: [
          Image.asset(
            member.image,
            height: 24,
            width: 24,
          ),
          SizedBox(width: 8),
          Text(
            member.name.split(' ').first,
            style: TextStyle(
              letterSpacing: 0,
              color: Colors.black,
              fontSize: 14,
            ),
          ),
          SizedBox(width: 15),
          InkWell(
            onTap: onTap,
            child: SvgPicture.asset(
              AssetImages.closeIcon,
              width: 15,
              height: 15,
            ),
          ),
        ],
      ),
    );
  }
}
