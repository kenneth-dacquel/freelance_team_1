import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/add_members/models/member.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class MembersSection extends StatefulWidget {
  const MembersSection({
    this.type,
    this.members,
  });

  final MemberType type;
  final List<Member> members;

  @override
  _MembersSectionState createState() => _MembersSectionState();
}

class _MembersSectionState extends State<MembersSection> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 25,
            vertical: 8,
          ),
          color: lightGrey.withOpacity(0.1),
          height: 32,
          width: MediaQuery.of(context).size.width,
          child: Text(
            widget.type == MemberType.CONTACT ? 'CONTACTS' : 'FOLLOWING',
            style: TextStyle(
              color: vulcan,
              fontSize: 13,
              fontWeight: FontWeight.w500,
              letterSpacing: 0,
            ),
            textAlign: TextAlign.start,
          ),
        ),
        SizedBox(height: 15),
        ...widget.members.map(
          (e) => Container(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: ListTile(
              leading: Image.asset(
                e.image,
                height: 50,
                width: 50,
              ),
              title: Row(
                children: [
                  Text(
                    e.name,
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      letterSpacing: 0,
                    ),
                  ),
                  SizedBox(width: 5),
                  if (e.isCertified)
                    SvgPicture.asset(
                      AssetImages.certifiedIcon,
                      width: 14,
                      height: 14,
                    ),
                ],
              ),
              subtitle: Text(
                e.subtitle,
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: lightGrey.withOpacity(0.5),
                  letterSpacing: 0,
                ),
              ),
              trailing: InkWell(
                child: SvgPicture.asset(
                  e.isChecked ? AssetImages.activeCheckbox2 : AssetImages.inactiveCheckbox,
                  height: 22,
                  width: 22,
                ),
                onTap: () => setState(() => e.isChecked = !e.isChecked),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
