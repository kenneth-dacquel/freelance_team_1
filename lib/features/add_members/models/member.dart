class Member {
  Member({
    this.name,
    this.subtitle,
    this.image,
    this.isJoined,
    this.isContact,
    this.isCertified,
    this.isChecked,
  });

  String name;
  String subtitle;
  String image;
  bool isJoined;
  bool isContact;
  bool isCertified;
  bool isChecked;
}
