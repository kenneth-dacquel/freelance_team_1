import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/about_group/about_group_page.dart';
import 'package:freelance_team_1/features/group_details/widgets/about_button.dart';
import 'package:freelance_team_1/features/group_details/widgets/group_info.dart';
import 'package:freelance_team_1/features/group_details/widgets/members_layout.dart';
import 'package:freelance_team_1/features/group_details/widgets/private_group_details.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/enums.dart';
import 'package:freelance_team_1/widgets/dark_green_button.dart';

class UnjoinedGroupDetails extends StatelessWidget {
  static const route = 'unjoined_group_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Column(
          children: [
            Text(
              'Group Name',
              style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 16,
              ),
            ),
            Text(
              'Private Group',
              style: TextStyle(
                color: Colors.black.withOpacity(0.5),
                fontSize: 12,
              ),
            ),
          ],
        ),
        leading: IconButton(
          icon: SvgPicture.asset(AssetImages.backButtonIcon),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          IconButton(
            padding: const EdgeInsets.only(right: 10),
            icon: SvgPicture.asset(AssetImages.ellipsis),
            onPressed: () {},
          )
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.only(
          left: 33,
          right: 29,
        ),
        children: [
          GroupInfo(),
          SizedBox(height: 13),
          Text(
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            style: TextStyle(
              fontSize: 14,
              color: Colors.black,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 18),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              MembersLayout(
                total: '26k',
                type: GroupClassification.OTHER,
              ),
              SizedBox(width: 28),
              AboutButton(onTap: () => Navigator.pushNamed(context, AboutGroupPage.route)),
            ],
          ),
          ListTile(
            leading: Image.asset(
              AssetImages.membersPlaceholder,
              height: 25,
              width: 40,
              fit: BoxFit.fill,
            ),
            title: RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: 14,
                  color: Colors.black,
                ),
                children: [
                  TextSpan(text: 'Joined by '),
                  TextSpan(
                    text: 'Nicholaas Jaar ',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  TextSpan(text: 'and '),
                  TextSpan(
                    text: '8 others',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          DarkGreenButton(
            onTap: null,
            title: 'Join Group',
          ),
          SizedBox(height: 43),
          PrivateGroupDetails(),
          SizedBox(height: 20),
        ],
      ),
    );
  }
}
