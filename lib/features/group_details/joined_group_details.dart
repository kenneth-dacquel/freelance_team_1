import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/features/group_details/widgets/group_info.dart';
import 'package:freelance_team_1/features/group_details/widgets/group_tabs.dart';
import 'package:freelance_team_1/features/group_settings/group_settings_page.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/widgets/dark_green_button.dart';

class JoinedGroupDetails extends StatelessWidget {
  static const route = 'joined_group_page';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Group Name',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        leading: IconButton(
          icon: SvgPicture.asset(AssetImages.backButtonIcon),
          onPressed: () => Navigator.pop(context),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: IconButton(
              onPressed: () => Navigator.pushNamed(context, GroupSettingsPage.route),
              icon: SvgPicture.asset(AssetImages.settingsIcon),
            ),
          ),
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.only(
          left: 33,
          right: 29,
        ),
        children: [
          GroupInfo(),
          SizedBox(height: 28),
          GroupTabs(),
          SizedBox(height: 21),
          Container(
            height: 1,
            width: MediaQuery.of(context).size.width,
            color: divider,
          ),
          SizedBox(height: 21),
          DarkGreenButton(
            onTap: null,
            title: 'Add Post',
          ),
        ],
      ),
    );
  }
}
