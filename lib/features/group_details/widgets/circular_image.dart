import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class CircularImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 138,
      height: 138,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
          color: green,
          width: 4,
        ),
      ),
      child: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AssetImages.placeholder),
            fit: BoxFit.fill,
          ),
          shape: BoxShape.circle,
          border: Border.all(color: Colors.white, width: 3),
        ),
      ),
    );
  }
}
