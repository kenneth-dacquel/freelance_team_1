import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AboutButton extends StatelessWidget {
  const AboutButton({@required this.onTap});

  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        height: 44,
        width: 95,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: lightGrey.withOpacity(0.05),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'About',
              style: TextStyle(
                fontSize: 16,
                color: lightGrey,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(width: 5),
            SvgPicture.asset(
              AssetImages.rightArrowIcon,
              width: 5,
              height: 7,
            ),
          ],
        ),
      ),
    );
  }
}
