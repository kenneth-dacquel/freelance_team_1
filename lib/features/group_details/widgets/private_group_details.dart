import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';

class PrivateGroupDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SvgPicture.asset(
          AssetImages.privateGroup,
          width: 100,
          height: 100,
          color: lightGrey.withOpacity(0.3),
        ),
        SizedBox(height: 4),
        Text(
          'This group is private',
          style: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600,
            color: lightGrey,
          ),
        ),
        SizedBox(height: 4),
        Text(
          'Follow Group name to see their posts',
          style: TextStyle(
            fontSize: 16,
            color: lightGrey,
          ),
        ),
      ],
    );
  }
}
