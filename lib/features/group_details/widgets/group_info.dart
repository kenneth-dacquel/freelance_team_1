import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

import 'circular_image.dart';

class GroupInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 13),
        CircularImage(),
        SizedBox(height: 13),
        Text(
          'Group Name',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 21,
          ),
        ),
        SizedBox(height: 5),
        Text(
          'London, United Kingdom',
          style: TextStyle(
            color: semiLightGrey,
            fontSize: 16,
            fontWeight: FontWeight.w500,
          ),
        ),
      ],
    );
  }
}
