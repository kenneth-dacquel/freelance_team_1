import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class MembersLayout extends StatelessWidget {
  const MembersLayout({
    this.total,
    this.type,
  });

  final String total;
  final GroupClassification type;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: type == GroupClassification.MY_GROUP ? CrossAxisAlignment.start : CrossAxisAlignment.center,
      children: [
        Text(
          total,
          style: TextStyle(
            color: Colors.black,
            fontSize: 17,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          'members',
          style: TextStyle(
            color: lightGrey.withOpacity(0.8),
            fontSize: 16,
          ),
        ),
      ],
    );
  }
}
