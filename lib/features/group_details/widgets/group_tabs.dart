import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/about_group/about_group_page.dart';
import 'package:freelance_team_1/features/group_details/widgets/about_button.dart';
import 'package:freelance_team_1/features/group_details/widgets/members_layout.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/enums.dart';

class GroupTabs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        MembersLayout(
          total: '1',
          type: GroupClassification.MY_GROUP,
        ),
        Container(
          height: 44,
          width: 116,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: green.withOpacity(0.1),
          ),
          child: FlatButton.icon(
            icon: SvgPicture.asset(
              AssetImages.plusIcon,
              width: 12,
              height: 12,
              color: green,
            ),
            onPressed: null,
            label: Text(
              'Invite',
              style: TextStyle(
                fontSize: 16,
                color: green,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
        AboutButton(onTap: () => Navigator.pushNamed(context, AboutGroupPage.route)),
      ],
    );
  }
}
