import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/features/create_story/widgets/create_story_dialog.dart';
import 'package:freelance_team_1/features/gallery_page/models/image_model.dart';
import 'package:freelance_team_1/features/gallery_page/widgets/bottom_navigation_gallery.dart';
import 'package:freelance_team_1/features/gallery_page/widgets/gallery_grid.dart';
import 'package:freelance_team_1/features/gallery_page/widgets/gallery_options.dart';
import 'package:freelance_team_1/features/gallery_page/widgets/image_selection.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:dartx/dartx.dart';
import 'package:freelance_team_1/utilities/bottom_sheet_creator.dart';
import 'package:photo_manager/photo_manager.dart';

class GalleryPage extends StatefulWidget {
  static const String route = 'gallery-page';

  @override
  _GalleryPageState createState() => _GalleryPageState();
}

class _GalleryPageState extends State<GalleryPage> {
  List<ImageModel> selectedImages;
  ScrollController scrollController;
  List<AssetEntity> assets;
  List<ImageModel> images;

  Future<void> _fetchAssets() async {
    final permitted = await PhotoManager.requestPermission();
    if (permitted) {
      final albums = await PhotoManager.getAssetPathList(onlyAll: true);
      final recentAlbum = albums.first;
      final recentAssets = await recentAlbum.getAssetListRange(
        start: 0,
        end: 1000000,
      );
      setState(() {
        assets = recentAssets;
      });
    }
  }

  @override
  void initState() {
    _fetchAssets().then((_) {
      images = assets.map((e) => ImageModel(asset: e)).toList();
      selectedImages = images.filter((element) => element.isSelected).toList();
    });
    scrollController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () => Navigator.pop(context),
          icon: SvgPicture.asset(
            AssetImages.closeIcon,
            height: 30,
            width: 30,
          ),
        ),
        title: Text(
          'Create story',
          style: TextStyle(
            color: Colors.black,
            fontSize: 17,
            fontWeight: FontWeight.w700,
            letterSpacing: 0,
          ),
        ),
      ),
      body: images?.isNotEmpty == true
          ? Column(
              children: [
                SizedBox(height: 67),
                GalleryOptions(),
                SizedBox(height: 10),
                Expanded(
                  child: Stack(
                    children: [
                      GalleryGrid(
                        children: [
                          ...?images.map(
                            (e) => ImageSelection(
                              onSelect: () => setState(() {
                                e.isSelected = !e.isSelected;
                                if (e.isSelected) {
                                  selectedImages.add(e);
                                  if (selectedImages.length > 3) {
                                    scrollController.animateTo(
                                      scrollController.position.maxScrollExtent + 50.0,
                                      curve: Curves.easeOut,
                                      duration: const Duration(milliseconds: 300),
                                    );
                                  }
                                } else {
                                  selectedImages.remove(e);
                                }
                              }),
                              isSelected: e.isSelected,
                              count: selectedImages.indexOf(e) + 1,
                              asset: e.asset,
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: BottomNavigationGallery(
                          onNextPressed: () {
                            // Add handler if no image is selected
                            if (selectedImages?.isNotEmpty == true)
                              BottomSheetCreator(
                                context: context,
                                dialog: CreateStoryDialog(assets: [...?selectedImages.map((e) => e.asset).toList()]),
                                barrierColor: Colors.black,
                              ).showBottomSheet();
                          },
                          selectedImages: selectedImages,
                          scrollController: scrollController,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
          : Center(
              child: SizedBox(
                height: 20,
                width: 20,
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
