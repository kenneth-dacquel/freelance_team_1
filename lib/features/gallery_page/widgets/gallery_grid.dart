import 'package:flutter/material.dart';

class GalleryGrid extends StatelessWidget {
  const GalleryGrid({this.children});

  final List<Widget> children;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) / 3;
    final double itemWidth = size.width / 2;
    return GridView(
      padding: const EdgeInsets.only(bottom: 95),
      children: children,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        crossAxisSpacing: 3,
        mainAxisSpacing: 3,
        childAspectRatio: itemWidth / itemHeight,
      ),
      shrinkWrap: true,
    );
  }
}
