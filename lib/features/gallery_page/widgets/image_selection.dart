import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:photo_manager/photo_manager.dart';

class ImageSelection extends StatelessWidget {
  const ImageSelection({
    @required this.onSelect,
    this.isSelected = false,
    this.height = 150,
    this.count,
    this.asset,
  });

  final bool isSelected;
  final double height;
  final VoidCallback onSelect;
  final int count;
  final AssetEntity asset;
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<Uint8List>(
      future: asset.thumbData,
      builder: (_, snapshot) {
        final bytes = snapshot.data;
        if (bytes == null)
          return Center(
            child: SizedBox(
              height: 20,
              width: 20,
              child: CircularProgressIndicator(),
            ),
          );
        return InkWell(
          onTap: onSelect,
          child: Stack(
            children: [
              Container(
                height: 200,
                width: MediaQuery.of(context).size.width / 3,
                child: Image.memory(
                  bytes,
                  fit: BoxFit.cover,
                  gaplessPlayback: true,
                ),
              ),
              Positioned(
                top: 5,
                right: 5,
                child: Container(
                  height: 28,
                  width: 28,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(
                      color: Colors.white,
                      width: 1,
                    ),
                    color: isSelected ? green : Colors.transparent,
                  ),
                  child: Center(
                    child: Text(
                      isSelected ? count.toString() : '',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        letterSpacing: 0,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}
