import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/widgets/button_with_image.dart';

class GalleryOptions extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: 11,
        right: 8,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                'Recents',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                  color: Colors.black,
                  letterSpacing: 0,
                ),
              ),
              SizedBox(width: 5),
              SvgPicture.asset(AssetImages.arrowDownBlack),
            ],
          ),
          ButtonWithImage(
            onPressed: () => print('TEST'),
            label: 'Select multiple',
            image: SvgPicture.asset(AssetImages.galleryIcon),
          ),
        ],
      ),
    );
  }
}
