import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/gallery_page/models/image_model.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class BottomNavigationGallery extends StatelessWidget {
  const BottomNavigationGallery({
    @required this.onNextPressed,
    @required this.scrollController,
    this.selectedImages,
  });

  final VoidCallback onNextPressed;
  final List<ImageModel> selectedImages;
  final ScrollController scrollController;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: semiTransparentBlack,
      height: 95,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width / 3,
            child: ListView.separated(
              controller: scrollController,
              scrollDirection: Axis.horizontal,
              itemCount: selectedImages.length,
              shrinkWrap: true,
              separatorBuilder: (context, index) => SizedBox(width: 5),
              itemBuilder: (context, index) => FutureBuilder<Uint8List>(
                future: selectedImages[index].asset.thumbData,
                builder: (_, snapshot) {
                  final bytes = snapshot.data;
                  if (bytes == null)
                    return Center(
                      child: SizedBox(
                        height: 20,
                        width: 20,
                        child: CircularProgressIndicator(),
                      ),
                    );
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: Image.memory(
                      bytes,
                      fit: BoxFit.fill,
                      gaplessPlayback: true,
                      height: 50,
                      width: 40,
                    ),
                  );
                },
              ),
            ),
          ),
          Container(
            height: 57,
            width: 142,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(52),
              color: Colors.white,
              border: Border.all(color: chipGrey),
            ),
            child: FlatButton(
              onPressed: onNextPressed,
              child: Text(
                'Next',
                style: TextStyle(
                  fontSize: 16,
                  color: vulcan,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
