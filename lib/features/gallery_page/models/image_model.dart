import 'package:photo_manager/photo_manager.dart';

class ImageModel {
  ImageModel({
    this.isSelected = false,
    this.asset,
  });

  bool isSelected;
  AssetEntity asset;
}
