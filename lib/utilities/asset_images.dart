class AssetImages {
  AssetImages._();

  static String _getAssetIconSvg(String fileName) => 'assets/images/$fileName.svg';

  static String _getAssetIconPng(String fileName) => 'assets/images/$fileName.png';

  static String backButtonIcon = _getAssetIconSvg('backbutton');

  static String settingsIcon = _getAssetIconSvg('settings_icon');

  static String ellipsis = _getAssetIconSvg('ellipsis');

  static String placeholder = _getAssetIconPng('placeholder');

  static String rightArrowIcon = _getAssetIconSvg('right_arrow');

  static String rightArrowIcon2 = _getAssetIconSvg('right_arrow2');

  static String plusIcon = _getAssetIconSvg('plus_icon');

  static String membersPlaceholder = _getAssetIconPng('members_placeholder');

  static String privateGroup = _getAssetIconSvg('private_group');

  static String groupImage = _getAssetIconPng('group_icon');

  static String arrowDown = _getAssetIconSvg('arrow_down');

  static String arrowDownBlack = _getAssetIconSvg('arrow_down_black');

  static String forward = _getAssetIconSvg('forward');

  static String bell = _getAssetIconSvg('bell');

  static String plus = _getAssetIconSvg('plus');

  static String search = _getAssetIconSvg('search');

  static String memberIcons = _getAssetIconPng('member_icons');

  static String groupLogo = _getAssetIconPng('group_logo');

  static String activeCheckbox = _getAssetIconSvg('active_checkbox');

  static String activeCheckbox2 = _getAssetIconSvg('active_checkbox2');

  static String inactiveCheckbox = _getAssetIconSvg('inactive_checkbox');

  static String closeIcon = _getAssetIconSvg('close_icon');

  static String member1 = _getAssetIconPng('member1');

  static String member2 = _getAssetIconPng('member2');

  static String member3 = _getAssetIconPng('member3');

  static String member4 = _getAssetIconPng('member4');

  static String certifiedIcon = _getAssetIconSvg('certified_icon');

  static String mediaIcon = _getAssetIconSvg('media');

  static String starIcon = _getAssetIconSvg('star');

  static String saveMediaIcon = _getAssetIconSvg('save_media');

  static String verifiedIcon = _getAssetIconSvg('verified');

  static String chatGroupInfoPlaceholder = _getAssetIconPng('chat_group_info_placeholder');

  static String groupInfoEdit = _getAssetIconSvg('group_info_edit');

  static String linkIcon = _getAssetIconSvg('link');

  static String chatGroupAttachmentLink1 = _getAssetIconPng('chat_group_attachment_link1');

  static String chatGroupAttachmentLink2 = _getAssetIconPng('chat_group_attachment_link2');

  static String chatGroupAttachmentDoc1 = _getAssetIconPng('chat_group_attachment_doc1');

  static String chatGroupAttachmentDoc2 = _getAssetIconPng('chat_group_attachment_doc2');

  static String storyBackgroundImage = _getAssetIconPng('story_background_image');

  static String deleteIcon = _getAssetIconSvg('delete_icon');

  static String deleteIconClear = _getAssetIconSvg('delete_icon_clear');

  static String downloadIcon = _getAssetIconSvg('download_icon');

  static String downloadIconClear = _getAssetIconSvg('download_icon_clear');

  static String mapPinIcon = _getAssetIconSvg('map_pin_icon');

  static String mapPinIcon2 = _getAssetIconSvg('map_pin_icon2');

  static String tagIcon = _getAssetIconSvg('tag_icon');

  static String textIcon = _getAssetIconSvg('text_icon');

  static String sendIcon = _getAssetIconSvg('send_icon');

  static String taggedImage = _getAssetIconPng('tagged_image');

  static String storyThumbnail = _getAssetIconPng('story_image_thumbnail');

  static String storyThumbnail2 = _getAssetIconPng('story_image_thumbnail2');

  static String highlightImage = _getAssetIconPng('highlights_image');

  static String storyUserAvatar = _getAssetIconPng('story_user_avatar');

  static String storyArrowUp = _getAssetIconSvg('story_arrow_up');

  static String micIcon = _getAssetIconSvg('mic');

  static String plusCircle = _getAssetIconSvg('plus_circle');

  static String storyArrowDown = _getAssetIconSvg('story_arrow_down');

  static String eyeIcon = _getAssetIconSvg('eye');

  static String galleryIcon = _getAssetIconSvg('gallery_icon');

  static String cornerUpperRight = _getAssetIconSvg('corner_upper_right');

  static String galleryImage = _getAssetIconPng('gallery_image');

  static String alignCenterIcon = _getAssetIconSvg('align_center');

  static String colorsIcon = _getAssetIconPng('colors_icon');
}
