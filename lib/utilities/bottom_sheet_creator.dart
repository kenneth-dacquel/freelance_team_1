import 'package:flutter/material.dart';

class BottomSheetCreator {
  BottomSheetCreator({
    @required this.context,
    @required this.dialog,
    this.before,
    this.after,
    this.backgroundColor = Colors.transparent,
    this.barrierColor,
  });

  final BuildContext context;
  final Widget dialog;
  final VoidCallback after;
  final VoidCallback before;
  final Color backgroundColor;
  final Color barrierColor;

  void showBottomSheet() {
    if (this.before != null) {
      this.before();
    }
    showModalBottomSheet(
      context: this.context,
      builder: (_) => dialog,
      backgroundColor: backgroundColor,
      barrierColor: barrierColor,
      isScrollControlled: true,
    ).then((_) {
      if (after != null) {
        this.after();
      }
    });
  }
}
