import 'package:flutter/material.dart';

final textTheme = TextTheme(
  headline4: TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: Colors.black,
  ),
  headline5: TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.w600,
    color: Colors.black,
  ),
  subtitle1: TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    color: Colors.black,
  ),
  button: TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 16,
  ),
  bodyText1: TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 12,
  ),
  bodyText2: TextStyle(
    fontWeight: FontWeight.w600,
    fontSize: 10,
  ),
  caption: TextStyle(
    fontWeight: FontWeight.w400,
    fontSize: 14,
  ),
);
