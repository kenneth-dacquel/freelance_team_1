enum MemberType { CONTACT, FOLLOWING }

enum GroupClassification { MY_GROUP, OTHER }

enum StoryViewer { ALL, CONTACTS, FRIENDS, LOCATION }

enum AllowReplies { ALL, CONTACTS, FRIENDS }

enum EmojisType { RECENT, ALL }
