import 'package:flutter/material.dart';

final unselectedChipBackgroundColor = Color(0xFF12273E).withOpacity(.05);

final selectedChipBackgroundColor = Color(0xFF12273E);

final shadowColor = Color(0xFF12273E).withOpacity(.15);

final selectedChipTextColor = Color(0xFFFFFFFF);

final unselectedChipTextColor = Color(0xFF091828);

final vulcan = Color(0xff36383D);

final red = Color(0xff8E0000);

final green = Color(0xff00AE4D);

final darkGreen = Color(0xff057938);

final blue = Color(0xff135AC6);

final peach = Color(0xffFADCC2);

final gold = Color(0xffBCA900);

const lightGrey = Color(0xff627790);

const divider = Color(0xffE0E0E0);

const semiLightGrey = Color(0xff7D7D7D);

const chipGrey = Color(0xffC4C4C4);

const chipDark = Color(0xff070707);

const bottomSheetRed = Color(0xffE34E51);

const semiTransparentBlack = Color(0xD6000000);
