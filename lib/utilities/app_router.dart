import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/about_group/about_group_page.dart';
import 'package:freelance_team_1/features/add_address/add_address_page.dart';
import 'package:freelance_team_1/features/add_group/add_group_page.dart';
import 'package:freelance_team_1/features/add_members/add_members_page.dart';
import 'package:freelance_team_1/features/chat_group_attachment/chat_group_attachment_page.dart';
import 'package:freelance_team_1/features/chat_group_info/chat_group_info_page.dart';
import 'package:freelance_team_1/features/community/community_details_page.dart';
import 'package:freelance_team_1/features/community/community_page.dart';
import 'package:freelance_team_1/features/create_group/create_group_page.dart';
import 'package:freelance_team_1/features/create_story/create_story_page.dart';
import 'package:freelance_team_1/features/gallery_page/gallery_page.dart';
import 'package:freelance_team_1/features/group_details/joined_group_details.dart';
import 'package:freelance_team_1/features/group_details/unjoined_group_details.dart';
import 'package:freelance_team_1/features/group_members/group_members_page.dart';
import 'package:freelance_team_1/features/group_settings/group_settings_page.dart';
import 'package:freelance_team_1/features/story_settings/story_settings_page.dart';
import 'package:freelance_team_1/features/view_story/view_stories_page.dart';

final navigatorKey = GlobalKey<NavigatorState>();

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AddGroupPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => AddGroupPage());

      case AddAddressPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => AddAddressPage());

      case CommunityPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => CommunityPage());

      case CommunityDetailsPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => CommunityDetailsPage());

      case JoinedGroupDetails.route:
        return MaterialPageRoute<dynamic>(builder: (_) => JoinedGroupDetails());

      case UnjoinedGroupDetails.route:
        return MaterialPageRoute<dynamic>(builder: (_) => UnjoinedGroupDetails());

      case AboutGroupPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => AboutGroupPage());

      case GroupSettingsPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => GroupSettingsPage());

      case GroupMembersPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => GroupMembersPage());

      case AddMembersPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => AddMembersPage());

      case ChatGroupInfoPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => ChatGroupInfoPage());

      case CreateGroupPage.route:
        return MaterialPageRoute<dynamic>(builder: (_) => CreateGroupPage());

      case ChatGroupAttachmentPage.route:
        return MaterialPageRoute(builder: (_) => ChatGroupAttachmentPage());

      case CreateStoryPage.route:
        return MaterialPageRoute(builder: (_) => CreateStoryPage());

      case StorySettingsPage.route:
        return MaterialPageRoute(builder: (_) => StorySettingsPage());

      case ViewStoriesPage.route:
        final args = settings.arguments as ViewStoriesPageArgs;
        return MaterialPageRoute(builder: (_) => ViewStoriesPage(args: args));

      case GalleryPage.route:
        return MaterialPageRoute(builder: (_) => GalleryPage());

      default:
        return MaterialPageRoute<dynamic>(builder: (_) {
          return Scaffold(
            body: Center(child: Text('No route defined for ${settings.name}')),
          );
        });
    }
  }
}
