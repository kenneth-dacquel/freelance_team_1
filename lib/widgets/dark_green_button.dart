import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class DarkGreenButton extends StatelessWidget {
  const DarkGreenButton({
    @required this.onTap,
    this.title,
    this.radius = 30.0,
  });
  final VoidCallback onTap;
  final String title;
  final double radius;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: darkGreen,
      ),
      child: FlatButton(
        onPressed: onTap,
        child: Text(
          title,
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
