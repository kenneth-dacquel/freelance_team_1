import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final String label;
  final VoidCallback onPressed;
  final Color backgroundColor;
  final Color labelColor;

  const AppButton({
    Key key,
    @required this.label,
    @required this.onPressed,
    this.backgroundColor,
    this.labelColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return FlatButton(
      height: 50,
      minWidth: double.infinity,
      onPressed: onPressed,
      color: backgroundColor,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
      child: Text(
        label,
        style: textTheme.button.copyWith(color: labelColor),
      ),
    );
  }
}
