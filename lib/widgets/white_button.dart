import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class WhiteButton extends StatelessWidget {
  const WhiteButton({
    @required this.onTap,
    this.label,
  });

  final VoidCallback onTap;
  final String label;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 57,
      width: 333,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: Colors.white,
        border: Border.all(color: chipGrey),
      ),
      child: FlatButton(
        onPressed: onTap,
        child: Text(
          label,
          style: TextStyle(
            fontSize: 16,
            color: vulcan,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
