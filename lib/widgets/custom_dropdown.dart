import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';

class CustomDropdown<T> extends StatefulWidget {
  const CustomDropdown({
    this.onValueChanged,
    this.hint,
    this.items,
    this.initialValue,
    @required this.textBuilder,
  });

  final String hint;
  final List<T> items;
  final T initialValue;
  final Function(T value) onValueChanged;
  final String Function(T value) textBuilder;

  @override
  State<StatefulWidget> createState() => _CustomDropdownState();
}

class _CustomDropdownState<T> extends State<CustomDropdown<T>> {
  T value;

  @override
  void initState() {
    value = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 4,
        bottom: 2,
      ),
      decoration: BoxDecoration(
        color: value != null ? Color(0x32C4C4C4) : Colors.white,
        border: Border.all(color: Color(0x80627790)),
        borderRadius: BorderRadius.circular(8),
      ),
      child: DropdownButtonFormField<T>(
        value: value,
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: widget.hint,
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Color(0x8012273E),
          ),
        ),
        icon: SvgPicture.asset(AssetImages.arrowDown),
        style: TextStyle(
          color: Color(0xff12273E),
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        items: widget.items
            .map(
              (i) => DropdownMenuItem(
                child: Text(widget.textBuilder(i)),
                value: i,
              ),
            )
            .toList(),
        onChanged: (value) {
          setState(() => this.value = value);
          if (widget.onValueChanged != null) {
            widget.onValueChanged(value);
          }
        },
      ),
    );
  }
}
