import 'package:flutter/material.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class ButtonWithImage extends StatelessWidget {
  const ButtonWithImage({
    @required this.onPressed,
    this.label,
    this.image,
  });

  final VoidCallback onPressed;
  final String label;
  final Widget image;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        height: 37,
        padding: const EdgeInsets.symmetric(horizontal: 20),
        decoration: BoxDecoration(
          color: green,
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (image != null) image,
            SizedBox(width: 10),
            Text(
              label,
              style: TextStyle(
                color: Colors.white,
                fontSize: 14,
                fontWeight: FontWeight.w500,
                letterSpacing: 0,
              ),
            )
          ],
        ),
      ),
    );
  }
}
