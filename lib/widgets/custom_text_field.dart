import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    this.label,
    this.controller,
    this.onChanged,
  });

  final TextEditingController controller;
  final Function(String) onChanged;
  final String label;

  @override
  State<StatefulWidget> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool showFillColor = false;

  @override
  void initState() {
    showFillColor = widget.controller?.text?.isNotEmpty ?? false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        top: 2,
        bottom: 4,
      ),
      decoration: BoxDecoration(
        color: showFillColor ? Color(0x32C4C4C4) : Colors.white,
        border: Border.all(color: Color(0x80627790)),
        borderRadius: BorderRadius.circular(8),
      ),
      child: TextField(
        controller: widget.controller,
        onChanged: (text) {
          setState(() => showFillColor = text?.isNotEmpty ?? false);
          if (widget.onChanged != null) {
            widget.onChanged(text);
          }
        },
        decoration: InputDecoration(
          labelText: widget.label,
          labelStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Color(0x8012273E),
          ),
          border: InputBorder.none,
        ),
        style: TextStyle(
          color: Color(0xff12273E),
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
