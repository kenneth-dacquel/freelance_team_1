import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:freelance_team_1/utilities/asset_images.dart';
import 'package:freelance_team_1/utilities/colors.dart';

class LightCustomTextField extends StatefulWidget {
  const LightCustomTextField({
    this.onTap,
    this.hint,
    this.onChanged,
    this.isFocused = false,
    this.withSuffix = true,
  });

  final VoidCallback onTap;
  final Function(String) onChanged;
  final String hint;
  final bool isFocused;
  final bool withSuffix;

  @override
  State<StatefulWidget> createState() => _LightCustomTextFieldState();
}

class _LightCustomTextFieldState extends State<LightCustomTextField> {
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      padding: const EdgeInsets.symmetric(
        vertical: 4,
        horizontal: 14,
      ),
      decoration: BoxDecoration(
        color: lightGrey.withOpacity(0.1),
        border: Border.all(
          color: lightGrey.withOpacity(0.0),
        ),
        borderRadius: BorderRadius.circular(12),
      ),
      child: TextField(
        autofocus: widget.isFocused,
        controller: controller,
        onChanged: (text) {
          if (widget.onChanged != null) {
            widget.onChanged(text);
          }
        },
        onTap: () {
          widget.onTap();
          if (!widget.isFocused) {
            FocusScope.of(context).unfocus();
          }
        },
        decoration: InputDecoration(
          hintText: widget.hint ?? '',
          hintStyle: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w400,
            color: vulcan.withOpacity(0.6),
            letterSpacing: 0,
          ),
          border: InputBorder.none,
          suffixIcon: widget.withSuffix
              ? InkWell(
                  onTap: () {
                    controller.clear();
                    widget.onChanged('');
                  },
                  child: Container(
                    padding: const EdgeInsets.all(2),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: lightGrey.withOpacity(0.5),
                    ),
                    child: SvgPicture.asset(
                      AssetImages.closeIcon,
                      height: 15,
                      width: 15,
                      color: Colors.white,
                    ),
                  ),
                )
              : null,
          suffixIconConstraints: BoxConstraints.expand(
            width: 22,
            height: 22,
          ),
        ),
        style: TextStyle(
          color: Color(0xff12273E),
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }
}
