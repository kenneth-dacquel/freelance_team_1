import 'package:flutter/material.dart';

class PravasiAppBar extends AppBar {
  PravasiAppBar({
    String title,
    List<Widget> actions,
    Widget leading,
  }) : super(
          title: Text(
            title,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 16,
            ),
          ),
          leading: leading,
          actions: actions,
        );
}
