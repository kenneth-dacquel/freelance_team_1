import 'package:flutter/material.dart';
import 'package:freelance_team_1/features/create_story/create_story_page.dart';
import 'package:freelance_team_1/utilities/app_router.dart';
import 'package:freelance_team_1/utilities/colors.dart';
import 'package:freelance_team_1/utilities/text_theme.dart';

class PravasiVoiceApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus.unfocus(),
      child: MaterialApp(
        title: 'Pravasi App',
        theme: ThemeData(
          textTheme: textTheme,
          appBarTheme: AppBarTheme(
            color: Colors.white,
            centerTitle: true,
            elevation: 1,
          ),
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          shadowColor: shadowColor,
        ),
        home: Navigator(
          key: navigatorKey,
          initialRoute: CreateStoryPage.route,
          onGenerateRoute: AppRouter.generateRoute,
        ),
      ),
    );
  }
}
